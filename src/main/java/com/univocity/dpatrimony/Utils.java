package com.univocity.dpatrimony;

import org.apache.batik.anim.dom.*;
import org.apache.batik.transcoder.*;
import org.apache.batik.transcoder.image.*;
import org.apache.batik.util.*;
import org.apache.commons.io.*;
import org.slf4j.*;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.net.*;
import java.nio.charset.*;
import java.util.List;
import java.util.concurrent.*;

public class Utils {

	private static final Logger log = LoggerFactory.getLogger(Utils.class);

	public static String readTextFromResource(String resourcePath, Charset encoding) {
		return readTextFromInput(getInput(resourcePath), encoding);
	}

	public static List<String> readLinesFromResource(String resourcePath, Charset encoding) {
		return readLinesFromInput(getInput(resourcePath), encoding);
	}

	public static List<String> readLinesFromInput(InputStream in, Charset encoding) {
		try {
			return IOUtils.readLines(in, encoding);
		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				throw (RuntimeException) e;
			}
			throw new IllegalArgumentException("Unable to read contents from input", e);
		}
	}

	public static String readTextFromInput(InputStream in, Charset encoding) {
		try {
			StringBuilder out = new StringBuilder();

			for (String line : readLinesFromInput(in, encoding)) {
				out.append(line).append('\n');
			}
			if (out.length() > 0) {
				out.deleteCharAt(out.length() - 1);
			}
			return out.toString();
		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				throw (RuntimeException) e;
			}
			throw new IllegalArgumentException("Unable to read contents from input", e);
		}
	}

	public static <T> T get(String description, Future<T> future) {
		return get(description, future, null);
	}

	public static <T> T get(String description, Future<T> future, T defaultValue) {
		try {
			return future.get();
		} catch (InterruptedException ex) {
			log.warn("Process '" + description + "' interrupted", ex);
			Thread.currentThread().interrupt();
		} catch (ExecutionException ex) {
			log.error("Error executing '" + description + "'", ex);
		}
		return defaultValue;
	}

	private static InputStream getInput(String path) {
		try {
			if (path != null) {
				path = FilenameUtils.separatorsToUnix(path);
			}

			File file = new File(path);
			if (!file.exists()) {
				InputStream input = Utils.class.getResourceAsStream(path);
				if (input == null) {
					input = Utils.class.getClassLoader().getResourceAsStream(path);
				}
				if (input == null) {
					input = ClassLoader.getSystemResourceAsStream(path);
				}
				if (input == null) {
					throw new IllegalStateException("Unable to determine resource from given path: " + path);
				} else {
					return input;
				}
			} else {
				return new FileInputStream(file);
			}
		} catch (IOException e) {
			throw new IllegalStateException("Unable to read resource from given path " + path, e);
		}
	}

	/**
	 * Ensures a given argument is not null.
	 *
	 * @param o         the object to validate
	 * @param fieldName the description of the field
	 */
	public static final void notNull(Object o, String fieldName) {
		if (o == null) {
			throw new IllegalArgumentException(fieldName + " cannot be null");
		}
	}

	/**
	 * Ensures a given {@link CharSequence} argument is not null/empty/blank
	 *
	 * @param o         a character sequence
	 * @param fieldName the description of the field
	 */
	public static final void notBlank(CharSequence o, String fieldName) {
		notNull(o, fieldName);
		if (o.toString().trim().isEmpty()) {
			throw new IllegalArgumentException(fieldName + " cannot be blank");
		}
	}

	private static class MyTranscoder extends ImageTranscoder {
		private BufferedImage image = null;

		public BufferedImage createImage(int w, int h) {
			return image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		}

		public void writeImage(BufferedImage img, TranscoderOutput out) {
		}

		public BufferedImage getImage() {
			return image;
		}
	}

	public static ImageIcon loadSvgIcon(String imageUrl, int width, int height) {
		return new ImageIcon(loadSvg(imageUrl, width, height));
	}

	public static ImageIcon loadSvgIcon(String imageUrl, int width, int height, Color color) {
		return new ImageIcon(loadSvg(imageUrl, width, height, color));
	}

	private static File createCSS(String css) {
		FileWriter cssWriter = null;
		File cssFile;
		try {
			cssFile = File.createTempFile("svg-", ".css");
			cssFile.deleteOnExit();
			cssWriter = new FileWriter(cssFile);
			cssWriter.write(css);
		} catch (IOException e) {
			cssFile = null;
		} finally {
			if (cssWriter != null) {
				try {
					cssWriter.flush();
					cssWriter.close();
				} catch (IOException e) {
				}
			}
		}
		return cssFile;
	}

	public static BufferedImage loadSvg(String imageUrl, int width, int height) {
		return loadSvg(imageUrl, width, height, null);
	}

	public static String toHex(Color color) {
		return color == null ? null : "#" + Integer.toHexString(color.getRGB() & 0xffffff);
	}

	public static BufferedImage loadSvg(String imageUrl, int width, int height, Color color) {
		String hexColor = toHex(color);
		String css = "svg {" +
				(color != null ? "fill: " + hexColor + ";" : "") +
				"shape-rendering: geometricPrecision;" +
				"text-rendering:  geometricPrecision;" +
				"color-rendering: optimizeQuality;" +
				"image-rendering: optimizeQuality;" +
				"}";
		File cssFile = createCSS(css);

		MyTranscoder transcoder = new MyTranscoder();
		TranscodingHints hints = new TranscodingHints();
		hints.put(ImageTranscoder.KEY_WIDTH, (float) width);
		hints.put(ImageTranscoder.KEY_HEIGHT, (float) height);
		hints.put(ImageTranscoder.KEY_DOM_IMPLEMENTATION, SVGDOMImplementation.getDOMImplementation());
		hints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT_NAMESPACE_URI, SVGConstants.SVG_NAMESPACE_URI);
		hints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT, SVGConstants.SVG_SVG_TAG);
		hints.put(ImageTranscoder.KEY_XML_PARSER_VALIDATING, false);
		if (cssFile != null) {
			hints.put(ImageTranscoder.KEY_USER_STYLESHEET_URI, cssFile.toURI().toString());
		}

		transcoder.setTranscodingHints(hints);
		try {
			URL url = Utils.class.getResource(imageUrl);
			if (url != null) {
				transcoder.transcode(new TranscoderInput(url.openStream()), null);

			}
			return transcoder.getImage();
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	public static void launchTestWindow(JComponent component) {
		Main.initUI();
		JFrame out = testWindow();
		out.add(component, BorderLayout.CENTER);
		SwingUtilities.invokeLater(() -> out.setVisible(true));
	}

	public static JFrame testWindow() {
		JFrame out = new JFrame("test");

		out.setIconImage(Utils.loadSvg("/images/icon.svg", 128, 128));

		out.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		out.setBounds(0, 0, 800, 600);
		out.setLocationRelativeTo(null);

		out.setLayout(new BorderLayout());

		return out;
	}

}
