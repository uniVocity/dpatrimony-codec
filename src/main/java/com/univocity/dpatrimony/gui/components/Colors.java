package com.univocity.dpatrimony.gui.components;

import java.awt.*;

public class Colors {
	public static final Color TITLE_TEXT_COLOR = new Color(31, 35, 44);
	public static final Color COMPONENT_TEXT_COLOR = new Color(71, 75, 84);
	public static final Color BLUE_TEXT_COLOR = new Color(52,146,255);

	public static final Color LIGHT_BLUE = new Color(51, 169, 255);
	public static final Color DARK_BLUE = new Color(51, 119, 255);

	public static final Color LIGHT_RED = new Color(255, 136, 166);
	public static final Color DARK_RED = new Color(233, 66, 86);

	public static final Color LIGHT_BLACK = new Color(101, 130, 148);
	public static final Color DARK_BLACK = new Color(31, 35, 44);

	public static final Color LIGHT_GRAY = new Color(242, 242, 242);
	public static final Color DARK_GRAY = new Color(190, 190, 190);
}
