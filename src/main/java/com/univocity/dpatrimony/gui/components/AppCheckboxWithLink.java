package com.univocity.dpatrimony.gui.components;

import com.univocity.dpatrimony.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.html.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.function.*;

import static com.univocity.dpatrimony.gui.components.Colors.*;

public class AppCheckboxWithLink extends JPanel implements HyperlinkListener {

	private final JTextPane textPane;
	private final JCheckBox checkBox = new JCheckBox();
	public List<Consumer<String>> onClickListeners = new ArrayList<>();

	public AppCheckboxWithLink(String text, String linkedText, Color linkColor) {
		int idx = text.indexOf(linkedText);
		int middle = idx + linkedText.length();
		StringBuilder tmp = new StringBuilder();
		tmp.append(text, 0, idx);
		tmp.append("<u><a href=\"https://clicked\"");
		if (linkColor != null) {
			tmp.append(" style=\"color: ").append(Utils.toHex(linkColor)).append('\"');
		}
		tmp.append('>');
		tmp.append(text, idx, middle);
		tmp.append("</a></u>");
		tmp.append(text, middle, text.length());

		add(checkBox);

		textPane = new JTextPane();
		textPane.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, Boolean.TRUE);
		textPane.setFont(FontManager.COMPONENT_FONT);
		textPane.setForeground(COMPONENT_TEXT_COLOR);
		textPane.setContentType("text/html");
		textPane.setOpaque(false);
		textPane.setDocument(new HTMLDocument());
		textPane.setText(prettyText(tmp.toString(), "left"));
		textPane.setEditable(false);
		textPane.addHyperlinkListener(this);
		add(textPane);
	}

	public JCheckBox getCheckBox(){
		return checkBox;
	}

	public void addOnClickListener(Consumer<String> onClick) {
		onClickListeners.add(onClick);
	}

	@Override
	public void hyperlinkUpdate(HyperlinkEvent e) {
		if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
			onClickListeners.forEach((c) -> c.accept(e.getURL() == null ? null : e.getURL().toString()));
		}
	}

	public static String prettyText(String badText, String textAlign) {
		return "<html><div style=\"text-align: " + textAlign + ";\">" + badText.replace("|||", "<br>") + "</html>";
	}
}
