package com.univocity.dpatrimony.gui.components;

import com.univocity.dpatrimony.*;

import javax.swing.*;

import static com.univocity.dpatrimony.FontManager.*;
import static com.univocity.dpatrimony.gui.components.Colors.*;

public class TitleLabel extends JLabel {

	public TitleLabel(String description) {
		super(description);
		setFont(TITLE_TEXT_FONT);
		setForeground(TITLE_TEXT_COLOR);
	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new TitleLabel("la las lasl lasd la "));
	}
}