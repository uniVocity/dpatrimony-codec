package com.univocity.dpatrimony.gui.components.ui;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.geom.*;

public class RoundedInputDefaults {
	private static final int borderRadius = 10;

	public static Border getBorder(){
		return BorderFactory.createEmptyBorder(8, 16, 8, 16);
	}

	public static void installUI(JComponent c) {
		c.setOpaque(false);
		c.setBorder(getBorder());
		c.setFont(FontManager.RUNNING_TEXT_FONT.deriveFont(16f));
		c.setForeground(Colors.DARK_BLACK);

		if (c instanceof JTextComponent) {
			JTextComponent editor = (JTextComponent) c;
			editor.setSelectionColor(Colors.LIGHT_BLUE);
		}
	}

	public static Shape createBorder(Component c){
		return new RoundRectangle2D.Double(0, 0,
				c.getWidth() - 1,
				c.getHeight() - 1,
				borderRadius * 2, borderRadius * 2);
	}

	public static void paintBorder(Graphics g, JComponent c) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		Shape border = createBorder(c);

		g2d.setPaint(Color.WHITE);
		g2d.fill(border);

		g2d.setPaint(Colors.DARK_GRAY);
		g2d.draw(border);
	}
}
