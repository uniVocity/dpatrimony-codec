package com.univocity.dpatrimony.gui.components;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.ui.*;

import javax.swing.*;
import java.awt.*;

public class AppTextField extends JTextField {

	public AppTextField(){
		super();
		setUI(new RoundedFieldUI());
	}

	public static void main(String... args) {
		Main.initUI();
		JPanel panel = new JPanel(new FlowLayout());
		panel.add(new AppTextField());
		Utils.launchTestWindow(panel);

	}

}
