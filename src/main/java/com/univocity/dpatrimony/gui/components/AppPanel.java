package com.univocity.dpatrimony.gui.components;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.border.*;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class AppPanel extends JPanel {

	private JPanel contentPane;

	public AppPanel() {
		setBackground(Color.WHITE);
		setBorder(new RoundedCornerBorder());

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(20, 20, 20, 20));
		contentPane.setBackground(Color.WHITE);
		this.add(contentPane, BorderLayout.CENTER);
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public static void main(String... args) {
		Main.initUI();
		JPanel panel = new JPanel(new FlowLayout());
		panel.add(new AppPanel());
		Utils.launchTestWindow(panel);

	}
}

