package com.univocity.dpatrimony.gui.components.ui;

import javax.swing.*;
import javax.swing.plaf.basic.*;
import java.awt.*;

public class RoundedFieldUI extends BasicTextFieldUI {
	public void installUI(JComponent c) {
		super.installUI(c);
		RoundedInputDefaults.installUI(c);
	}

	protected void paintSafely(Graphics g) {
		RoundedInputDefaults.paintBorder(g, getComponent());
		super.paintSafely(g);
	}
}