package com.univocity.dpatrimony.gui.components;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.net.*;

public class ResizableIcon extends ImageIcon {

	public ResizableIcon(String filename, String description) {
		super(filename, description);
	}

	public ResizableIcon(String filename) {
		super(filename);
	}

	public ResizableIcon(URL location, String description) {
		super(location, description);
	}

	public ResizableIcon(URL location) {
		super(location);
	}

	public ResizableIcon(Image image, String description) {
		super(image, description);
	}

	public ResizableIcon(Image image) {
		super(image);
	}

	public ResizableIcon(byte[] imageData, String description) {
		super(imageData, description);
	}

	public ResizableIcon(byte[] imageData) {
		super(imageData);
	}

	public ResizableIcon() {
	}

	@Override
	public synchronized void paintIcon(Component c, Graphics g, int x, int y) {
		Image image = getImage();
		if (image == null) {
			return;
		}
		Insets insets = ((Container) c).getInsets();
		x = insets.left;
		y = insets.top;

		int w = c.getWidth() - x - insets.right;
		int h = c.getHeight() - y - insets.bottom;

		int iw = image.getWidth(c);
		int ih = image.getHeight(c);

		if (iw * h < ih * w) {
			iw = (h * iw) / ih;
			x += (w - iw) / 2;
			w = iw;
		} else {
			ih = (w * ih) / iw;
			y += (h - ih) / 2;
			h = ih;
		}

		ImageObserver io = getImageObserver();
		g.drawImage(image, x, y, w, h, io == null ? c : io);
	}

	@Override
	public int getIconWidth() {
		return 0;
	}

	@Override
	public int getIconHeight() {
		return 0;
	}
}