package com.univocity.dpatrimony.gui.components.ui;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;

import javax.swing.*;
import javax.swing.plaf.basic.*;

public class AppMenuUI extends BasicPopupMenuUI {

	public void installUI(JComponent c) {
		super.installUI(c);
		RoundedInputDefaults.installUI(c);

		c.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		c.setFont(FontManager.RUNNING_TEXT_FONT.deriveFont(16f));
		c.setForeground(Colors.DARK_BLACK);
		c.setOpaque(false);
	}

}
