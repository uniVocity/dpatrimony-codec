package com.univocity.dpatrimony.gui.components;

import com.univocity.dpatrimony.*;

import javax.swing.*;
import java.awt.*;

import static com.univocity.dpatrimony.gui.components.Colors.*;
import static java.awt.RenderingHints.*;

public class AppButton extends JButton {

	private String pathToIcon;

	private int borderRadius = 11;

	private final Color start;
	private final Color end;

	private Color gradientStart;
	private Color gradientEnd;

	private final boolean outlined;

	public static AppButton blue(String text) {
		return new AppButton(text, false, LIGHT_BLUE, DARK_BLUE);
	}

	public static AppButton blueOutlined(String text) {
		return new AppButton(text, true, LIGHT_BLUE, DARK_BLUE);
	}

	public static AppButton red(String text) {
		return new AppButton(text, false, LIGHT_RED, DARK_RED);
	}

	public static AppButton redOutlined(String text) {
		return new AppButton(text, true, LIGHT_RED, DARK_RED);
	}

	public static AppButton black(String text) {
		return new AppButton(text, false, LIGHT_BLACK, DARK_BLACK);
	}

	public static AppButton blackOutlined(String text) {
		return new AppButton(text, true, LIGHT_BLACK, DARK_BLACK);
	}

	private AppButton(String text, boolean outlined, Color start, Color end) {
		super(text);
		this.start = start;
		this.end = end;
		this.outlined = outlined;
		setContentAreaFilled(false);
		setOpaque(false);
		setContentAreaFilled(false);
		setFocusPainted(false);
		setOpaque(false);

		setFont(FontManager.COMPONENT_FONT.deriveFont(16f));

		setMargin(new Insets(8, 24, 8, 24));
		setEnabled(true);
	}

	public void setEnabled(boolean enabled){
		super.setEnabled(enabled);
		Color foreground = !isEnabled() ? DARK_GRAY : outlined ? end : Color.white;
		setForeground(foreground);
		if(pathToIcon != null){
			setIcon(Utils.loadSvgIcon(pathToIcon, 22, 22, foreground));
		}
	}

	public void setIcon(String pathToIcon){
		this.pathToIcon = pathToIcon;
		setEnabled(this.isEnabled());
	}

	private void updateColors(Color start, Color end) {
		this.gradientStart = start;
		this.gradientEnd = end;

		if (getModel().isArmed()) {
			this.gradientStart = new Color(start.getRed(), start.getGreen(), start.getBlue(), 190);
		} else if (getModel().isRollover() || getModel().isSelected()) {
			this.gradientStart = new Color(start.getRed(), start.getGreen(), start.getBlue(), 215);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		final Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHints(new RenderingHints(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON));

		updateColors(isEnabled() ? start : LIGHT_GRAY, isEnabled() ? end : DARK_GRAY);
		paintGradientRectangle(g2, true, 0, 0, getSize().width - 1, getSize().height - 1);

		if (outlined) {
			g.setColor(Color.white);
			g.fillRoundRect(1, 1, getSize().width - 2, getSize().height - 1, borderRadius, borderRadius);
		}

		super.paintComponent(g);
	}

	private void setGradient(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		updateColors(gradientStart, gradientEnd);
		g2.setPaint(new GradientPaint(new Point(0, 0), gradientStart, new Point(0, getHeight()), gradientEnd));
	}

	private void paintGradientRectangle(Graphics g, boolean filled, int x, int y, int width, int height) {
		setGradient(g);
		if (filled) {
			g.fillRoundRect(x, y, width, height, borderRadius, borderRadius);
		} else {
			g.drawRoundRect(x, y, width, height, borderRadius, borderRadius);
		}
	}

	@Override
	protected void paintBorder(Graphics g) {
		if (outlined) {
			if (getModel().isRollover() || getModel().isSelected()) {
				gradientStart = gradientEnd = start;
			}
			if (!getModel().isArmed()) {
				((Graphics2D) g).setStroke(new BasicStroke(2));
			}
		}

		paintGradientRectangle(g, false, 0, 0, getSize().width - 1, getSize().height - 1);
	}

	public static void main(String... args) {
		Main.initUI();
		JPanel panel = new JPanel(new FlowLayout());
		panel.add(AppButton.blue("Continue"));
		panel.add(AppButton.blueOutlined("Cancel"));
		panel.add(AppButton.redOutlined("Home"));
		panel.add(AppButton.red("Abort"));
		panel.add(AppButton.blackOutlined("Proceed"));
		panel.add(AppButton.black("Back"));
		Utils.launchTestWindow(panel);

	}
}

