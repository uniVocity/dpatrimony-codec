package com.univocity.dpatrimony.gui.components;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.border.*;
import com.univocity.dpatrimony.gui.components.ui.*;
import org.apache.commons.lang3.*;

import javax.swing.*;
import javax.swing.plaf.basic.*;
import javax.swing.text.*;
import java.awt.*;
import java.util.*;

import static com.univocity.dpatrimony.gui.components.Colors.*;

public class AppCombobox extends JComboBox<String> {
	private static final Color BACKGROUND = Color.WHITE;
	private static final Color FOREGROUND = DARK_BLACK;

	static {
		UIManager.put("ComboBox.foreground", FOREGROUND);
		UIManager.put("ComboBox.background", BACKGROUND);
		UIManager.put("ComboBox.selectionForeground", BLUE_TEXT_COLOR);
		UIManager.put("ComboBox.selectionBackground", BACKGROUND);

		UIManager.put("ComboBox.buttonDarkShadow", BACKGROUND);
		UIManager.put("ComboBox.buttonBackground", FOREGROUND);
		UIManager.put("ComboBox.buttonHighlight", FOREGROUND);
		UIManager.put("ComboBox.buttonShadow", FOREGROUND);

		UIManager.put("ComboBox.border", new RoundedCornerBorder());
	}

	public AppCombobox() {
		initUi();
	}

	public AppCombobox(String[] items) {
		super(items);
		initUi();
	}

	public AppCombobox(Vector<String> items) {
		super(items);
		initUi();
	}

	private void initUi() {
		Object o = getAccessibleContext().getAccessibleChild(0);
		if (o instanceof JComponent) {
			JComponent c = (JComponent) o;
			c.setBorder(new RoundedCornerBorder());
			c.setForeground(FOREGROUND);
			c.setBackground(BACKGROUND);
		}

		getEditor().getEditorComponent().setBackground(BACKGROUND);
		((JTextComponent)getEditor().getEditorComponent()).setSelectionColor(BLUE_TEXT_COLOR);
		setFont(FontManager.RUNNING_TEXT_FONT.deriveFont(16f));

		setRenderer(new BasicComboBoxRenderer() {
			{
				setBorder(RoundedInputDefaults.getBorder());
			}
		});

		initializeComboWidth();
	}

	private void initializeComboWidth() {
		int size = getModel().getSize();
		if (size > 0) {
			int longestValueLength = 0;
			for (int i = 0; i < size; i++) {
				String element = getModel().getElementAt(i);
				if (element != null && element.length() > longestValueLength) {
					longestValueLength = element.length();
				}
			}
			setPrototypeDisplayValue(StringUtils.repeat('1', longestValueLength + 1));
		}
	}

	public static void main(String... args) {
		Main.initUI();
		JPanel panel = new JPanel(new FlowLayout());
		panel.add(new AppCombobox(new String[]{"item 1", "item 2", "item 3"}));
		Utils.launchTestWindow(panel);

	}
}

