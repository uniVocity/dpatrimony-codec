package com.univocity.dpatrimony.gui.components;

import com.univocity.dpatrimony.*;

import javax.swing.*;

import static com.univocity.dpatrimony.FontManager.*;
import static com.univocity.dpatrimony.gui.components.Colors.*;


public class LogoLabel extends JLabel {

	public LogoLabel() {
		super(" dPatrimony");
		setFont(TITLE_TEXT_FONT.deriveFont(18f));
		setForeground(TITLE_TEXT_COLOR);
		setIcon(Utils.loadSvgIcon("/images/icon.svg", 48, 48));
	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new LogoLabel());

	}
}
