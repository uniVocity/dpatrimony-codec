package com.univocity.dpatrimony.gui.components;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.ui.*;

import javax.swing.*;
import java.awt.*;

public class AppTextArea extends JTextArea {

	public AppTextArea(){
		super();
		setUI(new RoundedTextAreaUI());
	}

	public static void main(String... args) {
		Main.initUI();
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(new AppTextArea(), BorderLayout.CENTER);
		Utils.launchTestWindow(panel);

	}

}
