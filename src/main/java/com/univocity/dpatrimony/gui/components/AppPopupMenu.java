package com.univocity.dpatrimony.gui.components;

import com.github.weisj.darklaf.components.*;
import com.univocity.dpatrimony.gui.components.ui.*;

import javax.swing.*;
import java.awt.*;

public class AppPopupMenu extends JPopupMenu {

	private JComponent child;

	public AppPopupMenu(JComponent child){
		this.child = child;
		setUI(new AppMenuUI());
		OverlayScrollPane scroll = new OverlayScrollPane(new JScrollPane(child));
		scroll.setBorder(null);
		add(scroll, BorderLayout.CENTER);
	}

	public JComponent getChild(){
		return child;
	}

	@Override
	public void paintBorder(Graphics g) {
		super.paintBorder(g);
		RoundedInputDefaults.paintBorder(g, this);
	}
}
