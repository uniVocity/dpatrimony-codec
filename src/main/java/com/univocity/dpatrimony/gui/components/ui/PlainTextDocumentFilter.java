package com.univocity.dpatrimony.gui.components.ui;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;

public class PlainTextDocumentFilter extends DocumentFilter {

	public static void install(Component textComponent){
		((AbstractDocument)((JTextComponent)textComponent).getDocument()).setDocumentFilter(new PlainTextDocumentFilter());
	}

	public static void install(JComboBox<?> editableCombobox){
		install(editableCombobox.getEditor().getEditorComponent());
	}

	private PlainTextDocumentFilter(){

	}

	@Override
	public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
		super.replace(fb, offset, length, text.replaceAll("[^a-zA-Z0-9\\s]", ""), attrs);
	}

	@Override
	public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
		super.remove(fb, offset, length);
	}

	@Override
	public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
		super.insertString(fb, offset, string, attr);
	}
}