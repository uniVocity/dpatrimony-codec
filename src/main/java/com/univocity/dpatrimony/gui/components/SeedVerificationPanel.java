package com.univocity.dpatrimony.gui.components;

import org.apache.commons.lang3.*;

import javax.swing.*;
import java.awt.*;

public class SeedVerificationPanel extends JPanel {

	private String seed;
	private String[] scrambled;

	private StringBuilder tmp = new StringBuilder();

	private final Runnable onMatch;

	public SeedVerificationPanel(Runnable onMatch) {
		if(onMatch == null){
			throw new IllegalStateException("OnMatch can't be null");
		}
		this.onMatch = onMatch;
	}

	public void setSeed(String seed) {
		this.seed = seed.trim();
		this.scrambled = this.seed.split(" ");

		int cols = scrambled.length % 2 == 0 ? 4 : 3;
		int rows = scrambled.length / cols;

		setLayout(new GridLayout(rows, cols, 5, 5));

		reset();
	}

	private AppButton createButton(String word) {
		AppButton out = AppButton.blackOutlined(word);
		out.addActionListener((e) -> {
			out.setEnabled(false);
			if (tmp.length() > 0) {
				tmp.append(' ');
			}
			tmp.append(word);
			if(matches()){
				onMatch.run();
			}
		});
		return out;
	}

	public boolean matches(){
		return tmp.toString().equals(seed);
	}

	public void reset() {
		ArrayUtils.shuffle(scrambled);

		this.tmp.setLength(0);
		this.removeAll();
		for (String word : scrambled) {
			this.add(createButton(word));
		}
		this.updateUI();
	}
}
