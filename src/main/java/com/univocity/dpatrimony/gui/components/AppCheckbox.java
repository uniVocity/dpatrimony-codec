package com.univocity.dpatrimony.gui.components;

import com.univocity.dpatrimony.*;

import javax.swing.*;

import static com.univocity.dpatrimony.gui.components.Colors.*;

public class AppCheckbox extends JCheckBox {
	public AppCheckbox(String text){
		super(text);
		setFont(FontManager.COMPONENT_FONT);
		setForeground(COMPONENT_TEXT_COLOR);
	}
}
