package com.univocity.dpatrimony.gui.components.border;

import com.univocity.dpatrimony.gui.components.*;
import com.univocity.dpatrimony.gui.components.ui.*;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.*;

public class RoundedCornerBorder extends AbstractBorder {

	@Override
	public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		Area border = new Area(RoundedInputDefaults.createBorder(c));

		if (c instanceof JPopupMenu) {
			g2.setPaint(c.getBackground());
			g2.fill(border);
		} else {
			Container parent = c.getParent();
			if (Objects.nonNull(parent)) {
				g2.setPaint(parent.getBackground());
				Area corner = new Area(new Rectangle2D.Float(x, y, width, height));
				corner.subtract(border);
				g2.fill(corner);
			}
		}
		g2.setPaint(Colors.DARK_GRAY);
		g2.draw(border);
		g2.dispose();
	}

	@Override
	public Insets getBorderInsets(Component c) {
		if (c instanceof JPopupMenu) {
			return new Insets(4, 8, 4, 8);
		}
		return new Insets(0, 0, 0, 0);
	}
}

