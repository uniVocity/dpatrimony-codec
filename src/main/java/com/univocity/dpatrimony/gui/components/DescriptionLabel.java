package com.univocity.dpatrimony.gui.components;

import com.univocity.dpatrimony.*;

import javax.swing.*;

import java.awt.*;

import static com.univocity.dpatrimony.FontManager.*;
import static com.univocity.dpatrimony.gui.components.Colors.*;

public class DescriptionLabel extends JLabel {

	public DescriptionLabel() {
		this("");
	}

	public DescriptionLabel(String description) {
		super(description);
		setFont(RUNNING_TEXT_FONT);
		setForeground(COMPONENT_TEXT_COLOR);
	}

	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		super.paint(g);
	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new DescriptionLabel("la las lasl lasd la "));
	}
}