package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.gui.components.*;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.image.*;

public abstract class BaseScreen extends JPanel {

	private JPanel titlePanel;
	private JPanel footerPanel;
	private JPanel centerPanel;

	private JPanel buttonPanel;

	private boolean initialized;

	public BaseScreen() {
		setLayout(new BorderLayout());
	}

	public synchronized BaseScreen initialize(){
		if(!initialized) {
			initialized = true;
			add(getTitlePanel(), BorderLayout.NORTH);
			add(getCenterPanel(), BorderLayout.CENTER);
			add(getFooterPanel(), BorderLayout.SOUTH);
		}
		return this;
	}

	private TitleLabel titleLabel;

	private TitleLabel getTitleLabel(){
		if(titleLabel == null){
			titleLabel = new TitleLabel(getScreenTitle());
		}
		return titleLabel;
	}

	private JPanel getTitlePanel() {
		if (titlePanel == null) {
			titlePanel = new JPanel(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.CENTER;
			c.weightx = 1.0;
			c.weighty = 1.0;
			c.insets = new Insets(20, 25, 10, 25);
			titlePanel.add(getTitleLabel(), c);
		}
		return titlePanel;
	}

	protected JPanel getFooterPanel() {
		if (footerPanel == null) {
			footerPanel = new JPanel(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.SOUTHEAST;
			c.weightx = 1.0;
			c.weighty = 1.0;
			c.insets = new Insets(0, 10, 10, 10);
			footerPanel.add(new LogoLabel(), c);
		}
		return footerPanel;
	}

	abstract JPanel getCentralPanel();

	private JComponent getCentralComponent() {
		int rows = 0;
		BufferedImage image = getCenterImage();
		JLabel centerImage = null;
		if (image != null) {
			centerImage = new JLabel(new ImageIcon(image));
			rows++;
		}

		JPanel centralPanel = getCentralPanel();
		if (centralPanel != null) {
			rows++;
		}
		JPanel out = new JPanel(new GridLayout(rows, 1));
		if (centralPanel != null) {
			out.add(centralPanel);
		}
		if (centerImage != null) {
			out.add(centerImage);
		}
		return out;
	}

	abstract String getScreenTitle();

	abstract String getDescriptionText();

	abstract BufferedImage getCenterImage();

	private DescriptionLabel lblDescription;

	private DescriptionLabel getLblDescription(){
		if(lblDescription == null){
			lblDescription = new DescriptionLabel(getDescriptionText());
			lblDescription.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblDescription;
	}

	public final void resetScreenContents(){
		getTitleLabel().setText(getScreenTitle());
		getLblDescription().setText(getDescriptionText());
		doResetScreenContents();
	}

	protected abstract void doResetScreenContents();

	JPanel getCenterPanel() {
		if (centerPanel == null) {
			centerPanel = new JPanel(new BorderLayout());

			centerPanel.add(getLblDescription(), BorderLayout.NORTH);
			centerPanel.setBorder(new EmptyBorder(0, 50, 0, 50));

			JComponent centralComponent = getCentralComponent();
			centerPanel.add(centralComponent, BorderLayout.CENTER);
			centerPanel.add(getButtonPanel(), BorderLayout.SOUTH);
		}
		return centerPanel;
	}


	private JPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel(new GridBagLayout());
			initButtonPanel(buttonPanel);
		}
		return buttonPanel;
	}

	abstract void initButtonPanel(JPanel buttonPanel);

	protected void initButtonPanel(AppButton... buttons) {
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.SOUTHEAST;
		c.weightx = 0;

		for (int i = 0; i < buttons.length; i++) {
			c.gridx = i;
			if (i > 0) {
				c.insets = new Insets(0, 10, 0, 0);
			}
			buttonPanel.add(buttons[i], c);
		}

	}

}
