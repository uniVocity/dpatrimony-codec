package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.gui.components.*;
import com.univocity.dpatrimony.seed.*;

import javax.swing.*;
import java.awt.*;

public abstract class AbstractSeedInputScreen extends BaseScreenWithNavigation {

	private SeedPhrasePanel seedPhrasePanel;
	private byte[] seedEntropy;

	@Override
	final public void doResetScreenContents() {
		getCentralPanel().clearSecretContent();
	}

	@Override
	final SeedPhrasePanel getCentralPanel() {
		if(seedPhrasePanel == null){
			seedPhrasePanel = new SeedPhrasePanel(e -> {

				if(e != null){
					updateMsgLabel(e.getMessage());
				} else {
					seedEntropy = getCentralPanel().getSeedEntropy();
					if(seedEntropy == null) {
						updateMsgLabel("Invalid seed phrase. Checksum doesn't match.");
						getBtConfirm().setEnabled(false);
					} else {
						String seedOk;
						try {
							seedOk = seedOkMessage();
							getBtConfirm().setEnabled(seedOk != null);
							updateMsgLabel(seedOk);
						} catch (Exception ex){
							getBtConfirm().setEnabled(false);
							updateMsgLabel(ex.getMessage());
						}
					}
				}
			});
		}
		return seedPhrasePanel;
	}

	public void updateMsgLabel(String text){
		msgLabel.setText(text);
	}

	public void setExpectedWordCount(int wordCount){
		getCentralPanel().setExpectedWordCount(wordCount);
	}

	abstract String seedOkMessage();

	private JPanel footer;
	private DescriptionLabel msgLabel;

	@Override
	protected final JPanel getFooterPanel() {
		if(footer == null) {
			footer = super.getFooterPanel();
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			footer.add(getMsgLabel(), c);
		}
		return footer;
	}

	private JLabel getMsgLabel(){
		if(msgLabel == null){
			msgLabel = new DescriptionLabel();

		}
		return  msgLabel;
	}

	@Override
	void initButtonPanel(JPanel buttonPanel) {
		initButtonPanel(getBtCancel(), getBtBack(), getBtConfirm());
	}

	public final Secret getSecret(String name) {
		return new Secret(name, seedEntropy, seedPhrasePanel.getSelectedLanguage());
	}

	public void setSelectedLanguage(String language){
		seedPhrasePanel.setSelectedLanguage(language);
	}

	public String getSelectedLanguage(){
		return seedPhrasePanel.getSelectedLanguage();
	}
}
