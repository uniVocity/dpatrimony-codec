package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;
import com.univocity.dpatrimony.seed.*;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.util.function.*;

public class EncodingSecretCodeVerificationScreen extends BaseScreenWithNavigation {

	private final Supplier<Secret> secret;

	private final Runnable onMatch;

	public EncodingSecretCodeVerificationScreen(Supplier<Secret> secret, Runnable onMatch) {
		if(secret == null){
			throw new IllegalStateException("secret supplier can't be null");
		}
		if(onMatch == null){
			throw new IllegalStateException("onMatch can't be null");
		}
		this.secret = secret;
		this.onMatch = onMatch;
	}

	@Override
	JPanel getCentralPanel() {
		JPanel out = new JPanel(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		out.add(getSeedVerificationPanel(), c);

		return out;
	}

	@Override
	String getScreenTitle() {
		return "Encoding secret verification";
	}


	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Please select each word in order to confirm you wrote your secret phrase correctly.</center></p>" +
				"<p><center>Make sure to keep it safe and secure. Your beneficiaries will need this phrase to restore any secrets you want to bequeath.</center></p>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return null;
	}

	@Override
	void initButtonPanel(JPanel buttonPanel) {
		initButtonPanel(getBtCancel(), getBtBack(), getBtReset(), getBtConfirm());
	}

	private AppButton btReset;

	private AppButton getBtReset() {
		if (btReset == null) {
			btReset = AppButton.blueOutlined("Reset");
			btReset.addActionListener((e) -> resetScreenContents());
		}
		return btReset;
	}

	private void updateSecret() {
		if(this.secret != null) {
			Secret secret = this.secret.get();
			if (secret != null) {
				getSeedVerificationPanel().setSeed(secret.getSeed());
			}
		}
	}

	private SeedVerificationPanel seedVerificationPanel;

	private SeedVerificationPanel getSeedVerificationPanel() {
		if (seedVerificationPanel == null) {
			seedVerificationPanel = new SeedVerificationPanel(onMatch);
			updateSecret();
		}
		return seedVerificationPanel;
	}


	public static void main(String... args) {
		Main.initUI();
		BaseScreen screen = new EncodingSecretCodeVerificationScreen(()->new Secret("test", Seed.generateEntropy(12), "english"), () -> System.exit(0));
		screen.initialize();
		Utils.launchTestWindow(screen);
	}

	@Override
	public void doResetScreenContents() {
		updateSecret();
		getSeedVerificationPanel().reset();
		getBtConfirm().setEnabled(false);
	}
}
