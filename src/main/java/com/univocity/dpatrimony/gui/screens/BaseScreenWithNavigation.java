package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.gui.components.*;

public abstract class BaseScreenWithNavigation extends BaseScreen{

	private AppButton btCancel;
	public final AppButton getBtCancel() {
		if (btCancel == null) {
			btCancel = AppButton.red("Cancel");
		}
		return btCancel;
	}

	private AppButton btBack;
	public final AppButton getBtBack() {
		if (btBack == null) {
			btBack = AppButton.redOutlined("Back");
		}
		return btBack;
	}

	private AppButton btNext;

	public final AppButton getBtNext() {
		if (btNext == null) {
			btNext = AppButton.blue("Next");
		}
		return btNext;
	}

	private AppButton btConfirm;

	public final AppButton getBtConfirm() {
		if (btConfirm == null) {
			btConfirm = AppButton.blue("Confirm");
			btConfirm.setEnabled(false);
		}
		return btConfirm;
	}

}
