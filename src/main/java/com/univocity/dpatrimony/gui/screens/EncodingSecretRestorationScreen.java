package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;

import java.awt.image.*;

public class EncodingSecretRestorationScreen extends AbstractSeedInputScreen {

	public EncodingSecretRestorationScreen() {
		getCentralPanel().setExpectedWordCount(12);
	}

	@Override
	String seedOkMessage() {
		return "All good! Press \"confirm\" to restore this secret and continue.";
	}

	@Override
	String getScreenTitle() {
		return "Type your secret phrase";
	}


	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Please write your secret phrase below in order to restore encoded secrets.</center></p>" +
				"<p><center>Remember to always keep it safe and secure. Beneficiaries will need this phrase to restore bequeathed secrets.</center></p>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return null;
	}


	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new EncodingSecretRestorationScreen());
	}
}
