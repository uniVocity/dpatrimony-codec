package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.image.*;

public class QRCodeScreen extends BaseScreenWithNavigation {

	private JPanel qrCodePanel;
	private JLabel qrCodeLabel;

	private String beneficiaryName;

	public QRCodeScreen() {
		getBtConfirm().setText("Finish");
		getBtConfirm().setEnabled(true);
	}

	@Override
	JPanel getCentralPanel() {
		if (qrCodePanel == null) {
			qrCodePanel = new JPanel();
			qrCodePanel.setLayout(new BorderLayout());
			qrCodePanel.add(getQrCodeLabel());
		}
		return qrCodePanel;
	}

	private JLabel getQrCodeLabel() {
		if (qrCodeLabel == null) {
			qrCodeLabel = new JLabel();
			qrCodeLabel.setHorizontalAlignment(JLabel.CENTER);
			qrCodeLabel.setBorder(new EmptyBorder(20, 20, 20, 20));
		}
		return qrCodeLabel;
	}

	@Override
	String getScreenTitle() {
		return "Import secret";
	}


	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Please scan the QR code below with the dPatrymony mobile app.</center></p>" +
				"<p><center>This will enable you to safely share your encrypted secret with " + beneficiaryName + ".</center></p>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return null;
	}

	@Override
	protected void doResetScreenContents() {

	}

	@Override
	void initButtonPanel(JPanel buttonPanel) {
		initButtonPanel(getBtBack(), getBtConfirm());
	}

	public void setData(String beneficiaryName, String secretJson) {
		this.beneficiaryName = beneficiaryName;
		try {
			getQrCodeLabel().setIcon(new ResizableIcon(QrCode.generateQRCodeImage(secretJson, 800, 800)));
		} catch (Exception e) {
			throw new IllegalStateException("Error generating QR code from " + secretJson, e);
		}
		resetScreenContents();
	}

	public static void main(String... args) {
		Main.initUI();
		QRCodeScreen s = new QRCodeScreen();
		s.initialize();
		s.setData("lala", "{\"n\":\"BENEFICIARY\",\"t\":\"DESCRIPTION\",\"d\":\"aa\",\"s\":\"sleep pilot only bicycle wide hundred oxygen coconut panel neutral answer coral length ready sell unable card repeat have gather expand stairs occur nerve\",\"w\":\"english\"}");
		Utils.launchTestWindow(s);
	}
}
