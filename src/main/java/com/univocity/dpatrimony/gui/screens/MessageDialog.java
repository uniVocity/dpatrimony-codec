package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class MessageDialog extends JDialog {

	private String choice;
	private JPanel contentPanel;
	private DescriptionLabel lblMessage;

	private JPanel buttonPanel;

	private MessageDialog(Component parent) {
		super(SwingUtilities.getWindowAncestor(parent));

		if (getParent() != null) {
			setIconImages(((Window) getParent()).getIconImages());
		}

		setModal(true);
		setLayout(new BorderLayout());
		add(getContentPanel(), BorderLayout.CENTER);
		add(getButtonPanel(), BorderLayout.SOUTH);
	}

	private JPanel getContentPanel() {
		if (contentPanel == null) {
			contentPanel = new JPanel(new FlowLayout());
			contentPanel.add(getLblMessage());
			contentPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		}
		return contentPanel;
	}

	private DescriptionLabel getLblMessage() {
		if (lblMessage == null) {
			lblMessage = new DescriptionLabel();
		}
		return lblMessage;
	}

	private JPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel(new FlowLayout());
			buttonPanel.setBorder(new EmptyBorder(0, 0, 5, 0));
		}
		return buttonPanel;
	}

	private static AppButton newButton(MessageDialog d, String content) {
		AppButton button = AppButton.blue(content);
		button.addActionListener(e -> {
			d.choice = content;
			d.dispose();
		});
		return button;
	}

	public static String askConfirmation(Component parent, String title, String message, String... options) {
		MessageDialog d = new MessageDialog(parent);

		d.setTitle(title);
		d.getLblMessage().setText(message);

		for (String option : options) {
			d.getButtonPanel().add(newButton(d, option));
		}

		d.pack();

		d.setLocationRelativeTo(d.getParent());
		d.setVisible(true);
		return d.choice;
	}

	public static void main(String... args) {
		Main.initUI();
		String out = MessageDialog.askConfirmation(null, "Confirm deletion", "Are you sure you want to delete secret '" + "secret" + "'?", "Yes", "No");
		System.out.println(out);
	}
}
