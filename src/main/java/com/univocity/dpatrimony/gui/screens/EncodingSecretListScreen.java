package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;
import com.univocity.dpatrimony.seed.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.image.*;
import java.util.*;

public class EncodingSecretListScreen extends BaseScreen {

	private JTable tblSecrets;
	private DefaultTableModel tblSecretsModel;
	private JPanel panelSecrets;
	private AppButton btEncodeSecret;
	private AppButton btDecodeSecret;

	private AppButton btCreateSecret;
	private AppButton btRestoreSecret;
	private AppButton btDeleteSecret;

	private Map<String, Secret> secrets = new HashMap<>();

	private DefaultTableModel getTblSecretsModel() {
		if (tblSecretsModel == null) {
			tblSecretsModel = new DefaultTableModel(){
				@Override
				public Class<?> getColumnClass(int columnIndex) {
					return String.class;
				}
			};
		}
		return tblSecretsModel;
	}

	private JTable getTblSecrets() {
		if (tblSecrets == null) {
			tblSecrets = new JTable(getTblSecretsModel());

			tblSecrets.getTableHeader().setFont(FontManager.COMPONENT_FONT.deriveFont(Font.BOLD));
			tblSecrets.getTableHeader().setBackground(Colors.LIGHT_BLUE);
			tblSecrets.setFont(FontManager.RUNNING_TEXT_FONT.deriveFont(14f));
			tblSecrets.setForeground(Colors.DARK_BLACK);

			getTblSecretsModel().addColumn("Available encoding secrets");
//			getTblSecretsModel().addColumn("Secret");

			tblSecrets.setColumnSelectionAllowed(false);
			tblSecrets.setRowSelectionAllowed(true);
			tblSecrets.setCellSelectionEnabled(false);
			tblSecrets.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tblSecrets.setDefaultEditor(Object.class, null); //disable editing
			tblSecrets.setDefaultRenderer(String.class, new DefaultTableCellRenderer(){
				@Override
				public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
					super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
					setBorder(noFocusBorder);
					isSelected = tblSecrets.getSelectedRow() == row;
					if(isSelected){
						setBackground(Colors.DARK_BLUE);
						setForeground(Colors.LIGHT_GRAY);
					} else {
						setBackground(Color.WHITE);
						setForeground(Colors.DARK_BLACK);
					}
					return this;
				}
			});


			tblSecrets.getSelectionModel().addListSelectionListener(e -> {
				if(!e.getValueIsAdjusting()){
					getBtDeleteSecret().setEnabled(e.getFirstIndex() >= 0);
					getBtEncodeSecret().setEnabled(e.getFirstIndex() >= 0);
					getBtDecodeSecret().setEnabled(e.getFirstIndex() >= 0);
				}
			});
		}
		return tblSecrets;
	}

	private JPanel getPanelSecrets() {
		if (panelSecrets == null) {
			panelSecrets = new JPanel(new BorderLayout());
			panelSecrets.setBorder(new EmptyBorder(20, 110, 0, 0));

			panelSecrets.add(new JScrollPane(getTblSecrets()), BorderLayout.CENTER);

			JPanel buttons = new JPanel(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();

			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 0;
			c.insets = new Insets(5, 20, 5, 0);
			buttons.add(getBtCreateSecret(), c);

			c.gridy = 1;
			buttons.add(getBtRestoreSecret(), c);

			c.gridy = 2;
			buttons.add(getBtDeleteSecret(), c);

			panelSecrets.add(buttons, BorderLayout.EAST);
		}
		return panelSecrets;
	}

	public void addSecret(Secret secret){
		secrets.put(secret.getName(), secret);
		getTblSecretsModel().addRow(new Object[]{secret.getName()});
	}

	public AppButton getBtCreateSecret() {
		if (btCreateSecret == null) {
			btCreateSecret = AppButton.blue("Create");
		}
		return btCreateSecret;
	}

	public AppButton getBtRestoreSecret() {
		if (btRestoreSecret == null) {
			btRestoreSecret = AppButton.blueOutlined("Restore");
		}
		return btRestoreSecret;
	}

	public AppButton getBtDeleteSecret() {
		if (btDeleteSecret == null) {
			btDeleteSecret = AppButton.redOutlined("Delete");
			btDeleteSecret.setEnabled(false);
			btDeleteSecret.addActionListener(e->{
				int row = getTblSecrets().getSelectedRow();
				if(row == -1){
					getBtDeleteSecret().setEnabled(false);
				} else {
					String secretName = (String)getTblSecretsModel().getValueAt(row, 0);
					String answer = MessageDialog.askConfirmation(getTblSecrets(), "Confirm deletion", "Are you sure you want to delete secret '" + secretName + "'?", "Yes", "No");
					if("Yes".equals(answer)){
						getTblSecretsModel().removeRow(row);
						getBtDeleteSecret().setEnabled(false);
					}
				}
			});
		}
		return btDeleteSecret;
	}

	public Secret getSelectedSecret(){
		int row = getTblSecrets().getSelectedRow();
		if(row == -1){
			return null;
		}
		String secretName = String.valueOf(getTblSecretsModel().getValueAt(row, 0));
		return secrets.get(secretName);
	}

	public AppButton getBtEncodeSecret() {
		if (btEncodeSecret == null) {
			btEncodeSecret = AppButton.blue("Encode a secret");
			btEncodeSecret.setIcon("/icons/svg/lock-outline.svg");
			btEncodeSecret.setMargin(new Insets(8, 24, 8, 18));
			btEncodeSecret.setHorizontalTextPosition(SwingConstants.LEFT);
			btEncodeSecret.setEnabled(false);
		}
		return btEncodeSecret;
	}


	public AppButton getBtDecodeSecret() {
		if (btDecodeSecret == null) {
			btDecodeSecret = AppButton.blue("Decode a secret");
			btDecodeSecret.setIcon("/icons/svg/lock-open-variant-outline.svg");
			btDecodeSecret.setMargin(new Insets(8, 24, 8, 18));
			btDecodeSecret.setHorizontalTextPosition(SwingConstants.LEFT);
			btDecodeSecret.setEnabled(false);
		}
		return btDecodeSecret;
	}

	@Override
	JPanel getCentralPanel() {
		JPanel centerPanel = new JPanel(new BorderLayout());

		centerPanel.setBorder(new EmptyBorder(0, 50, 0, 50));
		centerPanel.add(getPanelSecrets(), BorderLayout.CENTER);
		return centerPanel;
	}

	@Override
	String getScreenTitle() {
		return "Encoding secrets";
	}

	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Please create or restore at least one encoding secret.</center></p>" +
				"<p><center>An encoding secret is a special key used to encrypt secrets you want to share. " +
				"Use it to securely encrypt current and future secrets to share with your beneficiaries</center></p>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return null;
	}

	@Override
	protected void doResetScreenContents() {
	}

	@Override
	void initButtonPanel(JPanel buttonPanel) {
		GridBagConstraints c = new GridBagConstraints();
		c.gridy = 1;
		c.insets = new Insets(20, 0, 0, 0);
		buttonPanel.add(getBtEncodeSecret(), c);
		c.insets = new Insets(20, 20, 0, 0);
		buttonPanel.add(getBtDecodeSecret(), c);
	}

	public static void main(String... args) {
		Main.initUI();
		EncodingSecretListScreen s = new EncodingSecretListScreen();
		s.initialize();
		s.addSecret(new Secret("a", Seed.generateEntropy(12), "english"));
		s.addSecret(new Secret("b", Seed.generateEntropy(12), "english"));
		s.addSecret(new Secret("c", Seed.generateEntropy(12), "english"));
		Utils.launchTestWindow(s);
	}
}
