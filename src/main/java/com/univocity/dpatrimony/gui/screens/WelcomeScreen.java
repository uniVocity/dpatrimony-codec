package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;

import static com.univocity.dpatrimony.gui.components.Colors.*;

public class WelcomeScreen extends BaseScreen {

	private AppButton btProceed;
	private AppCheckboxWithLink chkAcceptTerms;

	public AppButton getBtProceed() {
		if (btProceed == null) {
			btProceed = AppButton.blue("Proceed");
			btProceed.setIcon("/icons/svg/chevron-right.svg");
			btProceed.setMargin(new Insets(8, 24, 8, 18));
			btProceed.setHorizontalTextPosition(SwingConstants.LEFT);
			btProceed.setEnabled(false);
		}
		return btProceed;
	}

	private AppCheckboxWithLink getChkAcceptTerms() {
		if (chkAcceptTerms == null) {
			chkAcceptTerms = new AppCheckboxWithLink("I understand and accept the terms and conditions of usage of this software.", "terms and conditions", BLUE_TEXT_COLOR);
			chkAcceptTerms.getCheckBox().addActionListener(e -> getBtProceed().setEnabled(chkAcceptTerms.getCheckBox().isSelected()));
		}
		return chkAcceptTerms;
	}

	@Override
	BufferedImage getCenterImage() {
		return Utils.loadSvg("/images/welcome.svg", 200, 200);
	}

	@Override
	JPanel getCentralPanel() {
		return null;
	}

	@Override
	String getScreenTitle() {
		return "Welcome";
	}

	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Welcome to dPatrimony. This application is" +
				" intended to run on an airgapped system with no access to the internet" +
				" and no persistent storage.</center></p>" +
				"<p><center>Before proceeding, make sure no one is around" +
				" you or can record/see your screen.</center></p>" +
				"</html>";
	}

	@Override
	void initButtonPanel(JPanel buttonPanel) {
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		buttonPanel.add(getChkAcceptTerms(), c);
		c.gridy = 1;
		c.insets = new Insets(20, 0, 0, 0);
		buttonPanel.add(getBtProceed(), c);
	}

	@Override
	protected void doResetScreenContents() {

	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new WelcomeScreen());
	}
}
