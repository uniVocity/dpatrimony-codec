package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;
import com.univocity.dpatrimony.seed.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import static com.univocity.dpatrimony.FontManager.*;

public class EncodingSecretCodeScreen extends BaseScreenWithNavigation {

	private JPanel secretNamePanel;

	protected byte[] entropy;

	@Override
	JPanel getCentralPanel() {
		JPanel out = new JPanel(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		out.add(getTxtSecretContent(), c);

		c.gridy = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 0, 10, 0);
		out.add(getSecretNamePanel(), c);


		return out;
	}

	@Override
	String getScreenTitle() {
		return "This is your encoding secret";
	}

	@Override
	public void doResetScreenContents() {
		entropy = null;
		updateSecret();
	}

	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Write the secret code below in a piece of paper.</center></p>" +
				"<p><center>Make sure to keep it safe and secure. Your beneficiaries will need this phrase to restore any secrets you want to bequeath.</center></p>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return null;
	}

	@Override
	void initButtonPanel(JPanel buttonPanel) {
		initButtonPanel(getBtCancel(), getBtBack(), getBtNext());
	}

	private JPanel getSecretNamePanel() {
		if (secretNamePanel == null) {
			secretNamePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
			secretNamePanel.add(new DescriptionLabel("Language "));
			secretNamePanel.add(getCmbWordList());
		}
		return secretNamePanel;
	}

	void updateSecret() {
		if(getEntropy() != null) {
			String seed = Seed.generateSeedPhrase(getEntropy(), getSelectedLanguage());
			getTxtSecretContent().setText(seed);
		}
	}

	private AppTextArea txtSecretContent;

	private AppTextArea getTxtSecretContent() {
		if (txtSecretContent == null) {
			txtSecretContent = new AppTextArea();
			txtSecretContent.setFont(RUNNING_TEXT_FONT.deriveFont(18f));
			txtSecretContent.setForeground(Color.BLACK);
			txtSecretContent.setDisabledTextColor(Color.BLACK);
			txtSecretContent.setEditable(false);
			txtSecretContent.setRows(4);
			txtSecretContent.setWrapStyleWord(true);
			txtSecretContent.setLineWrap(true);
			txtSecretContent.setEnabled(false);
			txtSecretContent.setTransferHandler(null);
			txtSecretContent.setComponentPopupMenu(null);
			txtSecretContent.setInheritsPopupMenu(false);

			updateSecret();
		}
		return txtSecretContent;
	}

	public String getSelectedLanguage() {
		return (String) getCmbWordList().getSelectedItem();
	}

	public void setSelectedLanguage(String language){
		getCmbWordList().setSelectedItem(language);
	}

	private AppCombobox cmbWordList;

	private AppCombobox getCmbWordList() {
		if (cmbWordList == null) {
			cmbWordList = new AppCombobox(Seed.mnemonicSet);
			cmbWordList.setSelectedItem(0); //english
			cmbWordList.addItemListener((e) -> {
				if (e.getID() == ItemEvent.ITEM_STATE_CHANGED) {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						updateSecret();
					}
				}
			});
		}
		return cmbWordList;
	}

	public byte[] getEntropy() {
		if (entropy == null) {
			entropy = Seed.generateEntropy(12);
		}
		return entropy;
	}

	public Secret getSecret(String name){
		return new Secret(name, getEntropy(), getSelectedLanguage());
	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new EncodingSecretCodeScreen().initialize());
	}
}
