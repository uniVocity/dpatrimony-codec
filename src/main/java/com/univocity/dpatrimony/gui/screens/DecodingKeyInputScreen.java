package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.seed.*;

import java.awt.image.*;

public class DecodingKeyInputScreen extends AbstractSeedInputScreen {

	private Secret encodingSecret;


	@Override
	String getScreenTitle() {
		return "Encryption secret";
	}

	@Override
	String getDescriptionText() {
		return "" +
				"<html>" +
				"<p><center>Type the encryption secret phrase in the text area below.</p></center>" +
				"<p><center>This will be used to decrypt secrets bequeathed to you.</p></center>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return null;
	}

	@Override
	String seedOkMessage() {
		return "Press confirm to reveal the information bequeathed to you.";
	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new DecodingKeyInputScreen().initialize());
	}

	public void setData(Secret encodingSecret) {
		this.encodingSecret = encodingSecret;
		this.setSelectedLanguage(encodingSecret.getLanguage());
		resetScreenContents();
	}
}
