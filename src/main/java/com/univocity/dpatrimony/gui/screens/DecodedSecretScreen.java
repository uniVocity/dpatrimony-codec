package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.seed.*;

import javax.swing.*;
import java.awt.image.*;

public class DecodedSecretScreen extends EncodingSecretCodeScreen {

	public DecodedSecretScreen() {
		getBtConfirm().setText("Finish");
		getBtConfirm().setEnabled(true);
	}

	@Override
	String getScreenTitle() {
		return "Decoded secret";
	}

	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Write the secret code below in a piece of paper and make sure to keep it safe and secure. DO NOT store this in digital form such as pictures, e-mail, cloud services, etc.</center></p>" +
				"<p><center>Restore and transfer any amounts accessible using this secret to new wallets, devices or accounts that only you can control.</center></p>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return null;
	}

	@Override
	public void doResetScreenContents() {
		super.doResetScreenContents();
	}

	@Override
	public byte[] getEntropy() {
		return entropy;
	}

	@Override
	void initButtonPanel(JPanel buttonPanel) {
		initButtonPanel(getBtBack(), getBtConfirm());
	}

	public void setData(Secret decodedSecret){
		this.entropy = decodedSecret.getEntropy();
		this.setSelectedLanguage(decodedSecret.getLanguage());
		updateSecret();
	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new DecodedSecretScreen(){
			{
				entropy = Seed.generateEntropy(12);
			}
		}.initialize());
	}
}
