package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.seed.*;

import java.awt.image.*;

public class SecretEncodingScreen extends AbstractSeedInputScreen {

	private String beneficiaryName;
	private String secretDescription;
	private Secret encodingSecret;

	@Override
	String getScreenTitle() {
		return "Encoding a new secret";
	}

	@Override
	String getDescriptionText() {
		return "" +
				"<html>" +
				"<p><center>Type the seed phrase you want to share with " + beneficiaryName + " below.</p></center>" +
				"<p><center>It will be encrypted with secret \"" + (encodingSecret == null ? "" : encodingSecret.getName()) + "\"</p></center>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return null;
	}

	@Override
	String seedOkMessage() {
		return "Press confirm to generate a shareable secret for " + beneficiaryName;
	}

	public String getSecretQRCodeData() {
		Secret userInput = super.getSecret(beneficiaryName);
		AES aes = new AES(encodingSecret.getEntropy());
		String encodedSecret = aes.encrypt(userInput.getEntropy(), userInput.getLanguage());

		return "{" +
				"\"n\":\"" + beneficiaryName + "\"" +
				",\"t\":\"" + secretDescription + "\"" +
				",\"d\":\"" + encodingSecret.getName() + "\"" +
				",\"s\":\"" + encodedSecret + "\"" +
				",\"w\":\"" + userInput.getLanguage() + "\"" +
				"}";
	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new SecretEncodingScreen().initialize());
	}

	public void setData(String beneficiaryName, String secretDescription, Secret encodingSecret) {
		this.beneficiaryName = beneficiaryName;
		this.secretDescription = secretDescription;
		this.encodingSecret = encodingSecret;
		this.setSelectedLanguage(encodingSecret.getLanguage());
		resetScreenContents();
	}
}
