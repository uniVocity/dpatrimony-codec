package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;

public class RestoreEncodingSecretNameScreen extends EncodingSecretNameScreen {

	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Please provide a human readable name to be associated with the encryption key you are restoring.</center></p>" +
				"<p><center>This name will be shared with all secrets encoded with the encryption key.</center></p>" +
				"</html>";
	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new RestoreEncodingSecretNameScreen().initialize());
	}
}
