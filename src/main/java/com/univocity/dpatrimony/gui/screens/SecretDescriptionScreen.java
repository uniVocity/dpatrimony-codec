package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;
import com.univocity.dpatrimony.gui.components.ui.*;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

public class SecretDescriptionScreen extends BaseScreenWithNavigation {

	private AppTextField txtSecretDescription;

	public SecretDescriptionScreen() {

	}

	@Override
	JPanel getCentralPanel() {
		JPanel out = new JPanel(new GridBagLayout());
		out.setBorder(new EmptyBorder(50, 50, 50, 50));
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;
		out.add(txtSecretDescription(), c);
		return out;
	}

	private void updateButtons() {
		getBtNext().setEnabled(!getSecretDescription().isEmpty());
	}

	private AppTextField txtSecretDescription() {
		if (txtSecretDescription == null) {
			txtSecretDescription = new AppTextField();
			PlainTextDocumentFilter.install(txtSecretDescription);

			txtSecretDescription.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					SwingUtilities.invokeLater(() -> updateButtons());
				}

				@Override
				public void keyPressed(KeyEvent e) {
					SwingUtilities.invokeLater(() -> updateButtons());
				}
			});

		}
		return txtSecretDescription;
	}

	@Override
	String getScreenTitle() {
		return "Secret description";
	}

	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Please write a brief description of the secret you are going to share with your beneficiary.</center></p>" +
				"<p><center>This will help " + beneficiaryName + " to correctly restore your secret. Examples: \"ledger seed\", \"btc wallet\"</center></p>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return Utils.loadSvg("/images/wallet.svg", 200, 200);
	}

	public String getSecretDescription() {
		String out = txtSecretDescription.getText().trim();
		if (!out.isEmpty()) {
			return out;
		}
		return "";
	}

	@Override
	void initButtonPanel(JPanel buttonPanel) {
		initButtonPanel(getBtCancel(), getBtBack(), getBtNext());
		getBtNext().setEnabled(false);
	}

	private String beneficiaryName = "";

	public void setData(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
		resetScreenContents();
	}

	@Override
	public void doResetScreenContents() {
		txtSecretDescription().setText("");
	}

	public static void main(String... args) {
		Main.initUI();
		SecretDescriptionScreen s = new SecretDescriptionScreen();
		s.initialize();
		Utils.launchTestWindow(s);
	}


}
