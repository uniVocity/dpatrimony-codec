package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.seed.*;

import javax.swing.*;
import java.awt.image.*;

public class SecretDecodingScreen extends AbstractSeedInputScreen {

	private AES encodingSecret;

	@Override
	String getScreenTitle() {
		return "Secret decoding";
	}

	@Override
	String getDescriptionText() {
		return "" +
				"<html>" +
				"<p><center>Type the secret phrase in the text area below.</p></center>" +
				"<p><center>It can only be decoded using the correct encryption secret phrase your benefactor left you.</p></center>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return null;
	}

	@Override
	final void initButtonPanel(JPanel buttonPanel) {
		initButtonPanel(getBtCancel(), getBtConfirm());
	}

	public void setData(Secret encodingSecret) {
		this.encodingSecret = new AES(encodingSecret.getEntropy());
		this.setSelectedLanguage(encodingSecret.getLanguage());
		resetScreenContents();
	}

	String seedOkMessage(){
		getDecodedSecret();
		return "Secret decrypted successfully. Press \"confirm\" to reveal.";
	}

	public Secret getDecodedSecret(){
		byte[] decodedBytes = encodingSecret.decrypt(getSecret("").getEntropy());
		return new Secret("", decodedBytes, getSelectedLanguage());
	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new SecretDecodingScreen().initialize());
	}

}
