package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.image.*;

public class EncodingSecretNameScreen extends BaseScreenWithNavigation {

	private AppTextField txtSecretName;


	public EncodingSecretNameScreen() {

	}

	@Override
	public void doResetScreenContents() {
		txtSecretName.setText("");
	}

	@Override
	JPanel getCentralPanel() {
		JPanel out = new JPanel(new GridBagLayout());
		out.setBorder(new EmptyBorder(50, 50, 50, 50));
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;
		out.add(getTxtSecretName(), c);
		return out;
	}

	private AppTextField getTxtSecretName() {
		if (txtSecretName == null) {
			txtSecretName = new AppTextField();
		}
		return txtSecretName;
	}

	@Override
	String getScreenTitle() {
		return "Name your secret";
	}

	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Please provide a human readable name to be associated with the encryption key generated in the next screen.</center></p>" +
				"<p><center>This name will be shared with all secrets encoded with the encryption key.</center></p>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return Utils.loadSvg("/images/wallet.svg", 200, 200);
	}

	@Override
	void initButtonPanel(JPanel buttonPanel) {
		initButtonPanel(getBtCancel(), getBtNext());
	}

	public static void main(String... args) {
		Main.initUI();
		Utils.launchTestWindow(new EncodingSecretNameScreen().initialize());
	}

	public String getSecretName() {
		return txtSecretName.getText();
	}
}
