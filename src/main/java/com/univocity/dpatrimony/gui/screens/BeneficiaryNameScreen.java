package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.*;
import com.univocity.dpatrimony.gui.components.*;
import com.univocity.dpatrimony.gui.components.ui.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;

public class BeneficiaryNameScreen extends BaseScreenWithNavigation {

	private AppCombobox txtBeneficiary;
	private DefaultComboBoxModel<String> beneficiaryListModel;
	private final Map<String, String> beneficiaries = new TreeMap<>();

	public BeneficiaryNameScreen() {

	}

	@Override
	JPanel getCentralPanel() {
		JPanel out = new JPanel(new GridBagLayout());
		out.setBorder(new EmptyBorder(50, 50, 50, 50));
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;
		out.add(getCmbBeneficiary(), c);
		return out;
	}

	private void updateButtons() {
		String name = getBeneficiaryName();
		if (name != null && beneficiaries.containsKey(name.toLowerCase()) && !name.equals(getCmbBeneficiary().getSelectedItem())) {
			getCmbBeneficiary().setSelectedItem(beneficiaries.get(name.toLowerCase()));
		}

		getBtNext().setEnabled(name != null);

		Object selection = getCmbBeneficiary().getSelectedItem();
		//check selection for list instead of manually typed. If selected then allow deletion
		getBtRemove().setEnabled(selection != null && selection.equals(name));
	}

	private AppCombobox getCmbBeneficiary() {
		if (txtBeneficiary == null) {
			txtBeneficiary = new AppCombobox();
			txtBeneficiary.setModel(getBeneficiaryListModel());
			txtBeneficiary.setEditable(true);
			PlainTextDocumentFilter.install(txtBeneficiary);
			txtBeneficiary.addItemListener(e -> updateButtons());

			txtBeneficiary.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					SwingUtilities.invokeLater(() -> updateButtons());
				}

				@Override
				public void keyPressed(KeyEvent e) {
					SwingUtilities.invokeLater(() -> updateButtons());
				}
			});

		}
		return txtBeneficiary;
	}

	private DefaultComboBoxModel<String> getBeneficiaryListModel() {
		if (beneficiaryListModel == null) {
			beneficiaryListModel = new DefaultComboBoxModel<>();
		}
		return beneficiaryListModel;
	}

	@Override
	String getScreenTitle() {
		return "Name a beneficiary";
	}

	@Override
	String getDescriptionText() {
		return "<html>" +
				"<p><center>Please provide the name of the beneficiary who is going to receive your secrets.</center></p>" +
				"<p><center>The information provided in the next screen can only be decoded with your secret encoding key.</center></p>" +
				"</html>";
	}

	@Override
	BufferedImage getCenterImage() {
		return Utils.loadSvg("/images/wallet_balance_background.svg", 200, 200);
	}

	public String getBeneficiaryName() {
		String selection = (String) txtBeneficiary.getEditor().getItem();
		if (selection == null) {
			selection = (String) getCmbBeneficiary().getSelectedItem();

		}
		if (selection != null && selection.trim().length() > 1) {
			return selection.trim();
		}
		return null;
	}

	@Override
	void initButtonPanel(JPanel buttonPanel) {
		initButtonPanel(getBtCancel(), getBtRemove(), getBtNext());
		getBtNext().setEnabled(false);

		getBtNext().addActionListener(e -> {
			String beneficiary = getBeneficiaryName();
			if (beneficiary != null && !beneficiaries.containsKey(beneficiary.toLowerCase())) {
				beneficiaries.put(beneficiary.toLowerCase(), beneficiary);
				getBeneficiaryListModel().addElement(beneficiary);
			}
		});
	}


	private AppButton btRemove;

	private final AppButton getBtRemove() {
		if (btRemove == null) {
			btRemove = AppButton.redOutlined("Remove");
			btRemove.setEnabled(false);
			btRemove.addActionListener(e -> {
				String beneficiary = getBeneficiaryName();
				if (beneficiary != null) {
					String elementInList = beneficiaries.remove(beneficiary.toLowerCase());
					getBeneficiaryListModel().removeElement(elementInList);
					getBeneficiaryListModel().setSelectedItem(null);
				}
				getBtRemove().setEnabled(false);
			});
		}
		return btRemove;
	}


	@Override
	public void doResetScreenContents() {
		getCmbBeneficiary().setSelectedItem(null);
	}

	public static void main(String... args) {
		Main.initUI();
		BeneficiaryNameScreen s = new BeneficiaryNameScreen();
		s.initialize();
		Utils.launchTestWindow(s);
	}


}
