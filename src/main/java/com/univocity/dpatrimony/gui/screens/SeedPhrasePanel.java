package com.univocity.dpatrimony.gui.screens;

import com.univocity.dpatrimony.gui.components.*;
import com.univocity.dpatrimony.seed.*;
import com.univocity.dpatrimony.wordlist.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.function.*;

import static com.univocity.dpatrimony.FontManager.*;

public class SeedPhrasePanel extends JPanel {

	private Consumer<InvalidMnemonicException> invalidMnemonicHandler;

	public SeedPhrasePanel(Consumer<InvalidMnemonicException> invalidMnemonicHandler) {
		super(new GridBagLayout());

		this.invalidMnemonicHandler = invalidMnemonicHandler;
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		add(getTxtSecretContent(), c);

		c.gridy = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 0, 10, 0);
		add(getSeedLanguagePanel(), c);

	}

	private JPanel secretNamePanel;

	private JPanel getSeedLanguagePanel() {
		if (secretNamePanel == null) {
			secretNamePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
			secretNamePanel.add(new DescriptionLabel("Language "));
			secretNamePanel.add(getCmbWordList());
		}
		return secretNamePanel;
	}

	public void clearSecretContent() {
		getTxtSecretContent().setText("");
	}

	private SeedPhraseInput txtSecretContent;

	private List<String> updateWordList() {
		return Seed.mnemonicWordsForLanguage(getSelectedLanguage());
	}

	private SeedPhraseInput getTxtSecretContent() {
		if (txtSecretContent == null) {
			txtSecretContent = new SeedPhraseInput(updateWordList(), this::getSelectedLanguage);
			txtSecretContent.setFont(RUNNING_TEXT_FONT.deriveFont(18f));
			txtSecretContent.setForeground(Color.BLACK);
			txtSecretContent.setRows(4);

			txtSecretContent.setInvalidMnemonicHandler(invalidMnemonicHandler);
		}
		return txtSecretContent;
	}

	public byte[] getSeedEntropy() {
		try {
			return Seed.checkSeedPhrase(getTxtSecretContent().getText(), getSelectedLanguage());
		} catch (IllegalArgumentException e) {
			if (invalidMnemonicHandler != null) {
				invalidMnemonicHandler.accept(new InvalidMnemonicException());
				return null;
			} else {
				throw e;
			}
		}
	}

	public String getSelectedLanguage() {
		return (String) getCmbWordList().getSelectedItem();
	}

	private AppCombobox cmbWordList;

	private AppCombobox getCmbWordList() {
		if (cmbWordList == null) {
			cmbWordList = new AppCombobox(Seed.mnemonicSet);
			cmbWordList.setSelectedItem(0); //english
			cmbWordList.addItemListener((e) -> {
				if (e.getID() == ItemEvent.ITEM_STATE_CHANGED) {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						getTxtSecretContent().setWordList(updateWordList());
					}
				}
			});
		}
		return cmbWordList;
	}


	public void setExpectedWordCount(int expectedWordCount) {
		getTxtSecretContent().setExpectedWordCount(expectedWordCount);
	}

	public void setSelectedLanguage(String language) {
		getCmbWordList().setSelectedItem(language);
	}
}
