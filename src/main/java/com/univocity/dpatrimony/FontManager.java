package com.univocity.dpatrimony;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;

public class FontManager {

    public static Font getNormalFont() {
        return getFont("Lato-Regular");
    }

    public static Font getBoldFont() {
        return getFont("Lato-Bold");
    }

    public static Font getFont(String fontFileName) {
        InputStream is = FontManager.class.getResourceAsStream("/fonts/"+fontFileName+".ttf");
        try {
            return Font.createFont(Font.TRUETYPE_FONT, is);
        } catch (Exception e){
            throw new IllegalStateException(e);
        }
    }

    public static void setUIFont (javax.swing.plaf.FontUIResource f){
        Enumeration<Object> keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get (key);
            if (value instanceof javax.swing.plaf.FontUIResource)
                UIManager.put (key, f);
        }
    }

    public static final Font RUNNING_TEXT_FONT = getFont("OpenSans-Regular").deriveFont(14f);

    public static final Font COMPONENT_FONT = getFont("Mulish-Regular").deriveFont(16f);
    public static final Font TITLE_TEXT_FONT = FontManager.getFont("Poppins-Black").deriveFont(44f);

}