package com.univocity.dpatrimony;

import com.github.weisj.darklaf.*;
import com.github.weisj.darklaf.theme.*;
import com.univocity.dpatrimony.gui.screens.*;
import com.univocity.dpatrimony.seed.*;
import org.slf4j.*;

import javax.swing.*;
import javax.swing.plaf.nimbus.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

/*
initial screen with explanation
decode beneficiary secret
 */

public class Main extends JFrame {

	private static final Logger log = LoggerFactory.getLogger(Main.class);
	static boolean themeManagerLoaded = false;
	private static Theme defaultTheme;

	private JPanel centralPanel;
	private CardLayout cards;

	private static final String WELCOME = "WELCOME";
	private WelcomeScreen welcomeScreen;

	private static final String SECRETS_LIST = "SECRETS_LIST";
	private EncodingSecretListScreen encodingSecretListScreen;

	private static final String NEW_SECRET_NAME = "NEW_SECRET_NAME";
	private EncodingSecretNameScreen encodingSecretNameScreen;

	private static final String NEW_SECRET = "NEW_SECRET";
	private EncodingSecretCodeScreen encodingSecretCodeScreen;

	private static final String VALIDATE_SECRET = "VALIDATE_SECRET";
	private EncodingSecretCodeVerificationScreen encodingSecretCodeVerificationScreen;

	private static final String RESTORE_SECRET_NAME = "RESTORE_SECRET_NAME";
	private RestoreEncodingSecretNameScreen restoreEncodingSecretNameScreen;

	private static final String RESTORE_SECRET = "RESTORE_SECRET";
	private EncodingSecretRestorationScreen encodingSecretRestorationScreen;

	private static final String BENEFICIARY_NAME = "BENEFICIARY_NAME";
	private BeneficiaryNameScreen beneficiaryNameScreen;

	private static final String SECRET_DESCRIPTION = "SECRET_DESCRIPTION";
	private SecretDescriptionScreen secretDescriptionScreen;

	private static final String SECRET_ENCODING = "SECRET_ENCODING";
	private SecretEncodingScreen secretEncodingScreen;

	private static final String QR_CODE = "QR_CODE";
	private QRCodeScreen qrCodeScreen;

	private static final String DECODE_SECRET = "DECODE_SECRET";
	private SecretDecodingScreen secretDecodingScreen;

	private static final String SECRET_DECODED = "SECRET_DECODED";
	private DecodedSecretScreen decodedSecretScreen;

	public static void initUI() {
		if (defaultTheme != null) {
			return;
		}
		System.setProperty("awt.useSystemAAFontSettings", "on");
		System.setProperty("swing.aatext", "true");

		Locale.setDefault(new Locale("en", "US"));
		System.setProperty("user.language", "en");
		System.setProperty("user.country", "US");

		try {
			defaultTheme = new IntelliJTheme();
			LafManager.install(defaultTheme);
			LafManager.enabledPreferenceChangeReporting(true);

			Theme[] themes = LafManager.getRegisteredThemes();
			for (Theme theme : themes) {
				if (theme.getName().toLowerCase().contains("intellij")) {
					LafManager.replaceTheme(theme, theme.withDisplayName("Default light"));
					break;
				}
			}
			themeManagerLoaded = true;
		} catch (Exception e) {
			log.warn("Error initializing theme");
			try {
				UIManager.setLookAndFeel(new NimbusLookAndFeel());
			} catch (Exception ex) {
				log.warn("Error initializing looking and feel", e);
			}
		}

		boolean isMacOs = (System.getProperty("os.name").toLowerCase().contains("mac"));
		if (isMacOs) {
			InputMap im = (InputMap) UIManager.get("TextField.focusInputMap");
			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), DefaultEditorKit.copyAction);
			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), DefaultEditorKit.pasteAction);
			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), DefaultEditorKit.cutAction);
		}
	}


	public Main() {
		super("dPatrimony");

		setIconImage(Utils.loadSvg("/images/icon.svg", 128, 128));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 800, 600);
		setLocationRelativeTo(null);

		setLayout(new BorderLayout());

		add(getCentralPanel(), BorderLayout.CENTER);
	}

	private CardLayout getCards() {
		if (cards == null) {
			cards = new CardLayout();
		}
		return cards;
	}

	private Map<String, JComponent> screens = new HashMap<>();

	private void registerScreen(String name, JComponent screen) {
		if (screen instanceof BaseScreen) {
			((BaseScreen) screen).initialize();
		}
		getCentralPanel().add(name, screen);
		screens.put(name, screen);
	}

	private JPanel getCentralPanel() {
		if (centralPanel == null) {
			centralPanel = new JPanel(getCards());
			registerScreen(WELCOME, getWelcomeScreen());
			registerScreen(SECRETS_LIST, getEncodingSecretListScreen());
			registerScreen(NEW_SECRET_NAME, getEncodingSecretNameScreen());
			registerScreen(NEW_SECRET, getEncodingSecretCodeScreen());
			registerScreen(VALIDATE_SECRET, getEncodingSecretCodeVerificationScreen());
			registerScreen(RESTORE_SECRET_NAME, getRestoreEncodingSecretNameScreen());
			registerScreen(RESTORE_SECRET, getEncodingSecretRestorationScreen());
			registerScreen(BENEFICIARY_NAME, getBeneficiaryNameScreen());
			registerScreen(SECRET_DESCRIPTION, getSecretDescriptionScreen());
			registerScreen(SECRET_ENCODING, getSecretEncodingScreen());
			registerScreen(QR_CODE, getQRCodeScreen());
			registerScreen(DECODE_SECRET, getSecretDecodingScreen());
			registerScreen(SECRET_DECODED, getDecodedSecretScreen());
			getCards().show(getCentralPanel(), WELCOME);
		}
		return centralPanel;
	}

	private void displayScreen(String name) {
		displayScreen(name, false);
	}

	private void displayScreen(String name, boolean resetScreenContents) {
		if (resetScreenContents) {
			for (JComponent s : screens.values()) {
				if (s instanceof BaseScreenWithNavigation) {
					((BaseScreenWithNavigation) s).resetScreenContents();
				}
			}
		}
		getCards().show(getCentralPanel(), name);

	}

	private WelcomeScreen getWelcomeScreen() {
		if (welcomeScreen == null) {
			welcomeScreen = new WelcomeScreen();
			welcomeScreen.getBtProceed().addActionListener(e -> displayScreen(SECRETS_LIST));
		}
		return welcomeScreen;
	}

	private EncodingSecretListScreen getEncodingSecretListScreen() {
		if (encodingSecretListScreen == null) {
			encodingSecretListScreen = new EncodingSecretListScreen();
			encodingSecretListScreen.getBtCreateSecret().addActionListener(e -> displayScreen(NEW_SECRET_NAME));
			encodingSecretListScreen.getBtRestoreSecret().addActionListener(e -> displayScreen(RESTORE_SECRET_NAME));
			encodingSecretListScreen.getBtEncodeSecret().addActionListener(e-> displayScreen(BENEFICIARY_NAME, true));
			encodingSecretListScreen.getBtDecodeSecret().addActionListener(e-> {
				getSecretDecodingScreen().setData(getEncodingSecretListScreen().getSelectedSecret());
				displayScreen(DECODE_SECRET, true);
			});
		}
		return encodingSecretListScreen;
	}

	private RestoreEncodingSecretNameScreen getRestoreEncodingSecretNameScreen() {
		if (restoreEncodingSecretNameScreen == null) {
			restoreEncodingSecretNameScreen = new RestoreEncodingSecretNameScreen();
			restoreEncodingSecretNameScreen.getBtCancel().addActionListener(e -> displayScreen(SECRETS_LIST, true));
			restoreEncodingSecretNameScreen.getBtNext().addActionListener(e -> displayScreen(RESTORE_SECRET));
		}
		return restoreEncodingSecretNameScreen;
	}

	private EncodingSecretRestorationScreen getEncodingSecretRestorationScreen() {
		if (encodingSecretRestorationScreen == null) {
			encodingSecretRestorationScreen = new EncodingSecretRestorationScreen();
			encodingSecretRestorationScreen.getBtCancel().addActionListener(e -> displayScreen(SECRETS_LIST, true));
			encodingSecretRestorationScreen.getBtBack().addActionListener(e -> displayScreen(RESTORE_SECRET_NAME));
			encodingSecretRestorationScreen.getBtConfirm().addActionListener(e -> confirm(getEncodingSecretRestorationScreen().getSecret(getRestoreEncodingSecretNameScreen().getSecretName())));

		}
		return encodingSecretRestorationScreen;
	}

	private EncodingSecretNameScreen getEncodingSecretNameScreen() {
		if (encodingSecretNameScreen == null) {
			encodingSecretNameScreen = new EncodingSecretNameScreen();
			encodingSecretNameScreen.getBtCancel().addActionListener(e -> displayScreen(SECRETS_LIST, true));
			encodingSecretNameScreen.getBtNext().addActionListener(e -> displayScreen(NEW_SECRET));
		}
		return encodingSecretNameScreen;
	}

	private EncodingSecretCodeScreen getEncodingSecretCodeScreen() {
		if (encodingSecretCodeScreen == null) {
			encodingSecretCodeScreen = new EncodingSecretCodeScreen();
			encodingSecretCodeScreen.getBtCancel().addActionListener(e -> displayScreen(SECRETS_LIST, true));
			encodingSecretCodeScreen.getBtBack().addActionListener(e -> displayScreen(NEW_SECRET_NAME));
			encodingSecretCodeScreen.getBtNext().addActionListener(e -> {
				getEncodingSecretCodeVerificationScreen().resetScreenContents();
				displayScreen(VALIDATE_SECRET);
			});
		}
		return encodingSecretCodeScreen;
	}


	private EncodingSecretCodeVerificationScreen getEncodingSecretCodeVerificationScreen() {
		if (encodingSecretCodeVerificationScreen == null) {
			encodingSecretCodeVerificationScreen = new EncodingSecretCodeVerificationScreen(() -> getEncodingSecretCodeScreen().getSecret(getEncodingSecretNameScreen().getSecretName()), () -> getEncodingSecretCodeVerificationScreen().getBtConfirm().setEnabled(true));
			encodingSecretCodeVerificationScreen.getBtBack().addActionListener(e -> displayScreen(NEW_SECRET));
			encodingSecretCodeVerificationScreen.getBtCancel().addActionListener(e -> displayScreen(SECRETS_LIST, true));
			encodingSecretCodeVerificationScreen.getBtConfirm().addActionListener(e -> confirm(getEncodingSecretCodeScreen().getSecret(getEncodingSecretNameScreen().getSecretName())));
		}
		return encodingSecretCodeVerificationScreen;
	}

	private void confirm(Secret secret) {
		getEncodingSecretListScreen().addSecret(secret);
		displayScreen(SECRETS_LIST, true);
	}

	public BeneficiaryNameScreen getBeneficiaryNameScreen(){
		if(beneficiaryNameScreen == null){
			beneficiaryNameScreen = new BeneficiaryNameScreen();
			beneficiaryNameScreen.getBtCancel().addActionListener(e-> displayScreen(SECRETS_LIST, true));
			beneficiaryNameScreen.getBtNext().addActionListener(e-> {
				getSecretDescriptionScreen().setData(getBeneficiaryNameScreen().getBeneficiaryName());
				displayScreen(SECRET_DESCRIPTION);
			});

		}
		return beneficiaryNameScreen;
	}

	public SecretDescriptionScreen getSecretDescriptionScreen(){
		if(secretDescriptionScreen == null){
			secretDescriptionScreen = new SecretDescriptionScreen();
			secretDescriptionScreen.getBtBack().addActionListener(e -> displayScreen(BENEFICIARY_NAME));
			secretDescriptionScreen.getBtCancel().addActionListener(e-> displayScreen(SECRETS_LIST, true));
			secretDescriptionScreen.getBtNext().addActionListener(e-> {
				getSecretEncodingScreen().setData(getBeneficiaryNameScreen().getBeneficiaryName(),  getSecretDescriptionScreen().getSecretDescription(), getEncodingSecretListScreen().getSelectedSecret());
				displayScreen(SECRET_ENCODING);
			});
		}
		return secretDescriptionScreen;
	}

	public SecretEncodingScreen getSecretEncodingScreen() {
		if(secretEncodingScreen == null){
			secretEncodingScreen = new SecretEncodingScreen();

			secretEncodingScreen.getBtBack().addActionListener(e -> displayScreen(SECRET_DESCRIPTION));
			secretEncodingScreen.getBtCancel().addActionListener(e-> displayScreen(SECRETS_LIST, true));
			secretEncodingScreen.getBtConfirm().addActionListener(e-> {
				getQRCodeScreen().setData(getBeneficiaryNameScreen().getBeneficiaryName(), getSecretEncodingScreen().getSecretQRCodeData());
				displayScreen(QR_CODE);
			});
		}
		return secretEncodingScreen;
	}

	public QRCodeScreen getQRCodeScreen(){
		if(qrCodeScreen == null){
			qrCodeScreen = new QRCodeScreen();
			qrCodeScreen.getBtConfirm().addActionListener(e -> displayScreen(SECRETS_LIST, true));
			qrCodeScreen.getBtBack().addActionListener(e-> displayScreen(SECRET_ENCODING));
		}
		return qrCodeScreen;
	}

	private SecretDecodingScreen getSecretDecodingScreen(){
		if(secretDecodingScreen == null){
			secretDecodingScreen = new SecretDecodingScreen();
			secretDecodingScreen.getBtCancel().addActionListener(e -> displayScreen(SECRETS_LIST, true));
			secretDecodingScreen.getBtConfirm().addActionListener(e -> {
				getDecodedSecretScreen().setData(getSecretDecodingScreen().getDecodedSecret());
				displayScreen(SECRET_DECODED);
			});
		}
		return secretDecodingScreen;
	}

	private DecodedSecretScreen getDecodedSecretScreen(){
		if(decodedSecretScreen == null){
			decodedSecretScreen = new DecodedSecretScreen();
			decodedSecretScreen.getBtConfirm().addActionListener(e -> displayScreen(SECRETS_LIST, true));
			decodedSecretScreen.getBtBack().addActionListener(e -> displayScreen(DECODE_SECRET, true));
		}
		return decodedSecretScreen;
	}

	public static void main(String... args) {
		Main.initUI();
		Main main = new Main();
		SwingUtilities.invokeLater(() -> main.setVisible(true));
	}
}


