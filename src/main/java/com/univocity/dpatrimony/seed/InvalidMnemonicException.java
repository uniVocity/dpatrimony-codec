package com.univocity.dpatrimony.seed;

import org.apache.commons.lang3.*;

import java.util.*;

public class InvalidMnemonicException extends IllegalArgumentException {

	private final Set<String> unknownWords = new LinkedHashSet<>();
	private final int actualWordCount;
	private final int expectedWordCount;

	public InvalidMnemonicException() {
		super("Not a valid seed phrase. Checksum failed.");
		this.actualWordCount = -1;
		this.expectedWordCount = -1;
	}

	public InvalidMnemonicException(int expectedWordCount, int actualWordCount) {
		super(actualWordCount > 0 ? "Seed phrase length should be " + expectedWordCount + " words instead of " + actualWordCount : "Seed phrase cannot be blank");
		this.actualWordCount = actualWordCount;
		this.expectedWordCount = expectedWordCount;
	}

	private static String printMessage(Collection<String> unknownWords) {
		String words = unknownWords.toString();
		words = StringUtils.remove(words, "[");
		words = StringUtils.removeEnd(words, "]");
		return "Unknown mnemonic word" + (unknownWords.size() > 1 ? "s" : "") + ": " + words;
	}

	public InvalidMnemonicException(int expectedWordCount, int actualWordCount, Collection<String> unknownWords) {
		super(printMessage(unknownWords));
		this.unknownWords.addAll(unknownWords);
		this.actualWordCount = actualWordCount;
		this.expectedWordCount = expectedWordCount;
	}

	public Set<String> getUnknownWords() {
		return Collections.unmodifiableSet(unknownWords);
	}

	public int getActualWordCount() {
		return actualWordCount;
	}

	public int getExpectedWordCount() {
		return expectedWordCount;
	}
}
