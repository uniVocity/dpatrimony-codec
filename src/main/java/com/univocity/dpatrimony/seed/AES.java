package com.univocity.dpatrimony.seed;

import org.bouncycastle.jce.provider.*;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.math.*;
import java.security.*;
import java.util.*;

public class AES {

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	private final Cipher cipher;
	private final SecretKeySpec key;

	private final IvParameterSpec iv;

	public AES(String seed, String language) {
		this(Seed.checkSeedPhrase(seed, language));
	}

	public AES(byte[] keyBytes) {
		keyBytes = keyBytes.clone();
		try {
			cipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");
			key = new SecretKeySpec(keyBytes, "AES");

			Random random = new Random(new BigInteger(keyBytes).longValue());
			random.nextBytes(keyBytes);

			SeedScrambler.scramble(new BigInteger(keyBytes).intValue() % 50_000 + 50_000, random, keyBytes);

			random = new Random(new BigInteger(keyBytes).longValue());
			random.nextBytes(keyBytes);

			iv = new IvParameterSpec(keyBytes);
		} catch (Exception e) {
			throw new IllegalStateException("Error loading AES cipher", e);
		}
	}

	public byte[] encrypt(byte[] bytes) {
		return process("encrypt", Cipher.ENCRYPT_MODE, bytes);
	}

	public byte[] decrypt(byte[] bytes) {
		return process("decrypt", Cipher.DECRYPT_MODE, bytes);
	}

	private byte[] process(String action, int mode, byte[] bytes) {
		try {
			bytes = bytes.clone();
			cipher.init(mode, key, iv);
			return cipher.doFinal(bytes);
		} catch (Exception e) {
			throw new IllegalArgumentException("Could not " + action + " secret.", e);
		}
	}

	public String encrypt(byte[] bytes, String language) {
		return Seed.generateSeedPhrase(encrypt(bytes), language);
	}

	public String encrypt(String seed, String language) {
		return Seed.generateSeedPhrase(encrypt(Seed.checkSeedPhrase(seed, language)), language);
	}

	public String decrypt(String encryptedSeed, String language) {
		return Seed.generateSeedPhrase(decrypt(Seed.checkSeedPhrase(encryptedSeed, language)), language);
	}

}