package com.univocity.dpatrimony.seed;

public class Secret {

	private String name;
	private final byte[] entropy;
	private String language;
	private String seed;

	public Secret(String name, byte[] entropy, String language) {
		this.name = name;
		this.language = language;
		this.entropy = entropy.clone();
		this.seed = Seed.generateSeedPhrase(entropy, language);
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getEntropy() {
		return entropy.clone();
	}


	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
		this.seed = Seed.generateSeedPhrase(entropy, language);
	}

	public String getSeed() {
		return seed;
	}
}
