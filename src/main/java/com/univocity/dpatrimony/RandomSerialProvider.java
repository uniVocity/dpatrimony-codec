/*
 * Copyright (c) 2017 Univocity Software Pty Ltd. All rights reserved.
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 *
 */

package com.univocity.dpatrimony;


import com.univocity.dpatrimony.seed.*;

import java.util.*;

import static com.univocity.dpatrimony.seed.Seed.*;

/**
 * @author uniVocity Software Pty Ltd - <a href="mailto:dev@univocity.com">dev@univocity.com</a>
 */
public class RandomSerialProvider {

	public static final String A_Z = "0123456789ABCDEF";

	private static final char[] RANGE = (A_Z).toCharArray();
	private static final int MAX = RANGE.length;

	private static final String[] components = new String[MAX * MAX * MAX];

	static {
		if (components.length < 2048) {
			throw new IllegalStateException("Internal error");
		}
		int s = 0;
		for (int i = 0; i < MAX; i++) {
			for (int j = 0; j < MAX; j++) {
				for (int l = 0; l < MAX; l++) {
					components[s++] = new String(new char[]{RANGE[i], RANGE[j], RANGE[l]});
				}
			}
		}
	}


	public static String newSerial() {
		int[] seed = Seed.generateNumericSeed(9);
		seed = Arrays.copyOfRange(seed, 0, seed.length - 1); //removes checksum
		return newSerial(seed);
	}

	public static String newSerial(byte[] entropy) {
		return newSerial(generateNumbers(entropy));
	}

	public static String newSerial(int[] numbers) {
		StringBuilder tmp = new StringBuilder();

		for (int index : numbers) {
			tmp.append(components[index % components.length]);
		}

		StringBuilder out = new StringBuilder();
		for (int i = 0; i < tmp.length(); i++) {
			if (i > 0 && i % 6 == 0) {
				out.append('-');
			}
			out.append(tmp.charAt(i));
		}

		return out.toString();
	}


	public static void main(String... args) {
		System.out.println(newSerial());
		System.out.println(newSerial());
		System.out.println(newSerial());
		System.out.println(newSerial());
		System.out.println(newSerial());
		System.out.println(Seed.generateEnglishSeedPhrase(12));
	}
}
