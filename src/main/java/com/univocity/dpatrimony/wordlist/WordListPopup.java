package com.univocity.dpatrimony.wordlist;


import com.univocity.dpatrimony.gui.components.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class WordListPopup extends AppPopupMenu {

	private SeedPhraseInput textComponent;
	private final WordList list;
	private String wordFragment;
	private int insertionPosition;

	public WordListPopup(SeedPhraseInput textComponent, Collection<String> words) {
		this(textComponent, words.toArray(new String[0]));
	}

	public WordListPopup(SeedPhraseInput textComponent, String... words) {
		super(new WordList(words));
		this.list = (WordList) getChild();
		this.textComponent = textComponent;
		setPreferredSize(new Dimension(150, 200));

		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				insertSelection();
			}
		});
	}

	public void setWordList(Collection<String> words){
		list.setWordList(words);
	}

	public void setWordList(String ... words){
		list.setWordList(words);
	}

	public void moveUp() {
		int index = Math.min(list.getSelectedIndex() - 1, 0);
		selectIndex(index);
	}

	public void moveDown() {
		int index = Math.min(list.getSelectedIndex() + 1, list.getModel().getSize() - 1);
		selectIndex(index);
	}

	private void selectIndex(int index) {
		final int position = textComponent.getCaretPosition();
		list.setSelectedIndex(index);
		SwingUtilities.invokeLater(() -> textComponent.setCaretPosition(position));
	}

	public void show(int position, String wordFragment, Rectangle location) {
		if (list.refreshWordList(wordFragment)) {
			this.insertionPosition = position;
			this.wordFragment = wordFragment;
			show(textComponent, location.x, textComponent.getBaseline(0, 0) + location.y);
		}
	}

	public boolean insertSelection() {
		if (isVisible() && list.getSelectedItem() != null) {
			String selectedSuggestion = list.getSelectedItem().substring(wordFragment.length());
			textComponent.insertString(insertionPosition, selectedSuggestion);
			setVisible(false);
			textComponent.validateWords();
			return true;
		}
		return false;
	}
}