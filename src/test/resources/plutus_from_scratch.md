#### Setting up
Install NIX (https://nixos.org/download.html#download-nix)

```
pacman -S nix
sudo nano /etc/nix/nix.conf
```

Then add the following to enable the IOHK binary cache:
```
substituters        = https://hydra.iohk.io https://iohk.cachix.org https://cache.nixos.org/
trusted-public-keys = hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ= iohk.cachix.org-1:DpRUyj7h7V830dp/i6Nti+NEO2/nhblbov/8MW7Rqoo= cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=
```

Before doing anything make sure the nix-daemon is running
```
sudo systemctl enable nix-daemon     
sudo systemctl start nix-daemon   
sudo systemctl status nix-daemon     
```

Update nix channels (seems to be required for arch based distros)
```
sudo -i nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
sudo -i nix-channel --update nixpkgs
```

Clone IOHK's plutus-apps repository:
```
cd ~/dev/cardano/
git clone https://github.com/input-output-hk/plutus-apps

cd plutus-apps/
```

Checkout the correct tag (if applicable):
```
git checkout 3746610e53654a1167aeb4c6294c6096d16b0502
```

Then finally run (this may take a while on the first run):
```
nix-shell
```

From the shell CD into your code:
```
cd ~/dev/cardano/plutus-pioneer-program/code/week01/
cabal build
```

Also from the nix-shell, build and launch the documentation server:
```
build-and-serve-docs
```

On your browser, go to: http://0.0.0.0:8002/haddock

#### Build then start the plutus playground server

```
cd ~/dev/cardano/plutus-apps/
nix-shell

cd plutus-playground-client
plutus-playground-server
```

If nothing works try this:

```
cd ~/dev/cardano/plutus-apps/plutus-playground-server

cabal build
~/dev/cardano/plutus-apps/dist-newstyle/build/x86_64-linux/ghc-8.10.4.20210212/plutus-playground-server-0.1.0.0/x/plutus-playground-server/build/plutus-playground-server/plutus-playground-server psgenerator ./plutus-playground-client/generated

#start the server
plutus-playground-server

```

## If you see an error like this:

```
/nix/store/fm77y43lzpgnay4pyc48jwpln0lnmrk3-plutus-playground-server/bin/plutus-playground-server: line 10: /bin/plutus-playground-server: No such file or directory
```

Then edit aforementioned file to use the built binary directly:
```
sudo nano /nix/store/fm77y43lzpgnay4pyc48jwpln0lnmrk3-plutus-playground-server/bin/plutus-playground-server
```

And change the last line to call the binary:
```
#!/nix/store/can473ld4dc8izcjlm4i5daxsh1yl5d8-bash-4.4-p23/bin/bash
echo "plutus-playground-server: for development use only"
GHC_WITH_PKGS=$(nix-build --quiet --no-build-output -E '(import ./.. {}).plutus-apps.haskell.project.ghcWithPackages(ps: [ ps.plutus-core ps.plutus-tx ps.plutus-contract ps.plutus-ledger ps.playground-common ])')
export PATH=$GHC_WITH_PKGS/bin:$PATH

export FRONTEND_URL=https://localhost:8009
export WEBGHC_URL=http://localhost:8080
export GITHUB_CALLBACK_PATH=https://localhost:8009/api/oauth/github/callback

~/dev/cardano/plutus-apps/dist-newstyle/build/x86_64-linux/ghc-8.10.4.20210212/plutus-playground-server-0.1.0.0/x/plutus-playground-server/build/plutus-playground-server/plutus-playground-server webserver "$@"
```

Save and try to run the server again. It should work this time:
```
plutus-playground-server
```


sudo nano /nix/store/ry2hywgh6cmmm9d5b9566nxdskd752yc-plutus-playground-generate-purs/bin/plutus-playground-generate-purs


## Starting the client

On another shell, go to the client:
```
cd ~/dev/cardano/plutus-apps/
nix-shell
cd ~/dev/cardano/plutus-apps/plutus-playground-client

cd plutus-playground-client
npm run start
```

Then visit the playground on your localhost: https://localhost:8009

#### To use the simulator properly, we need to get the current time for it

Start the repl:

```
cd ~/dev/cardano/plutus-apps/
nix-shell
cd ~/dev/cardano/plutus-pioneer-program/code/week01

cabal repl

#import required modules:
import Data.Default
import Ledger.TimeSlot

# invoke the function slotToEndPOSIXTime with 10 to get the POSIX time of slot 10 
slotToEndPOSIXTime def 10

 > POSIXTime {getPOSIXTime = 1596059101999}

```