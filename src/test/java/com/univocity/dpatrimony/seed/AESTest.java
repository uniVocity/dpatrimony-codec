package com.univocity.dpatrimony.seed;

import org.testng.annotations.*;

import static org.testng.Assert.*;

public class AESTest {
	@Test
	public void testEncryptionAndDecryptionWorks(){
		byte[] original = Seed.generateEntropy(24);
		byte[] key = Seed.generateEntropy(12);

		AES aes = new AES(key);
		byte[] encrypted = aes.encrypt(original);

		String originalSeed = Seed.generateEnglishSeedPhrase(original);
		String encryptedSeed = Seed.generateEnglishSeedPhrase(encrypted);
		assertNotEquals(originalSeed, encryptedSeed);

		byte[] restored = aes.decrypt(encrypted);
		assertEquals(originalSeed, Seed.generateEnglishSeedPhrase(restored));

		aes = new AES(key);
		restored = aes.decrypt(encrypted);
		assertEquals(originalSeed, Seed.generateEnglishSeedPhrase(restored));
	}

	@Test
	public void testSeedDecryptionWorks(){
		String key = "reunion quit dilemma section dose casual key entry drama noble wasp monkey";
		String original = "gloom aerobic dune athlete million sudden swim claw immense desert brave clay satisfy tongue supreme tip crash pizza crucial topic faith brother expire tooth";
		String encrypted = "oil woman rigid license lab poem brass when document order visa issue decrease able infant sugar lawsuit guide welcome reflect pitch gas negative confirm kitten right vast eye season attend tackle valve asset claw round very";

		AES aes = new AES(key, "english");
		assertEquals(aes.decrypt(encrypted, "english"), original);

	}
}